<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	protected $fillable = ['jobTitle'];
	
    public function people()
    {
        return $this->belongsToMany(Person::Class);
    }

    public function category()
    {
    	return $this->belongsTo(Category::Class);
    }

    public function types()
    {
        return $this->hasMany(Type::Class);
    }
}
