<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Medication extends Model
{
    public function appointments()
    {
    	return $this->belongsToMany(Appointment::class);
    }
}
