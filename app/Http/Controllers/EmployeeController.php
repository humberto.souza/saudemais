<?php

namespace Clinic\Http\Controllers;

use Clinic\Http\Requests\StoreEmployee;
use Clinic\Http\Requests\UpdateEmployee;
use Clinic\Person;
use Clinic\Role;
use Clinic\User;
use Clinic\Expedient;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{

    public function index()
    {
        $employees = Person::people('employees');
        return view('employee.index',compact('employees'));
    }

    public function create()
    {
        if(\Auth::user()->person->roles->first()->id == 1)
        {
            $roles = Role::whereBetween('category_id', [1, 5])->get();
            $weekday = collect(['Segunda','Terça','Quarta','Quinta','Sexta','Sábado']);
            return view('employee.create', compact('roles','weekday'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function store(StoreEmployee $request)
    {   
        $person = Person::create($request->all());
        $person->addContact($request);
        $person->addRole($request);
        if($request->weekday != null)
            $person->addExpedient($request);
        
        $person->user()->create([
            'email' => $request->email,
            'password' => bcrypt($request->cpf)
        ]);

        session()->flash('message', 'Usuário cadastrado com sucesso.');
        session()->flash('alert', 'success');

        return redirect()->action('EmployeeController@index');
    }

    public function show(Person $person)
    {
        if($person->status == 0 && \Auth::user()->person->roles->first()->category_id != 1)
        {
            session()->flash('message','Usuário não encontrado.');
            session()->flash('alert','info');
            return back();
        }
        
        $weekday = array('Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado');

        $person->age();
        return view('employee.show',compact('person','weekday'));
    }

    public function edit(Person $person)
    {
        if(\Auth::user()->person->roles->first()->id < 2 || \Auth::user()->person->id == $person->id )
        {
            $roles = Role::all();
            $weekday = collect(['Segunda','Terça','Quarta','Quinta','Sexta','Sábado']);

            $e = collect($person->expedients);

            return view('employee.edit',compact('person','roles','weekday','e'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }  
    }

    public function update(UpdateEmployee $request, Person $person)
    {

        if(!$person->emailCheck($request->email,$person))
        {
            session()->flash('message', 'Este E-mail já está em uso.');
            session()->flash('alert', 'danger');
        
            return back();
        }

        /*if(!$person->cpfCheck($request->cpf,$person))
        {
            session()->flash('message', 'Este CPF já está em uso.');
            session()->flash('alert', 'danger');
        
            return back();
        }*/

        if(!$person->rgCheck($request->rg,$person))
        {
            session()->flash('message', 'Este RG já está em uso.');
            session()->flash('alert', 'danger');
        
            return back();
        }

        if($request->weekday)
        {
            for($i=0; $i < sizeOf($request->weekday); $i++) {

                if($request->start[$i] == null || $request->end[$i] == null)
                {
                    DB::table('expedients')
                        ->where('person_id',$person->id)
                        ->where('weekday', $request->weekday[$i])
                        ->delete();
                } else {

                    $result = Expedient::where('weekday', $request->weekday[$i])
                        ->where('person_id',$person->id)
                        ->update(['start' => $request->start[$i],'end' => $request->end[$i]]);

                    if($result == 0)
                    {
                        $person->addExpedient($request);
                    };

                }

            };
        };

        $employee = Person::find($person->id)->fill($request->all());
        $user = User::where('person_id',$employee->id)->first();
        $user->email = $request->email;
        $user->save();

        $employee->contact->fill($request->all())->save();
        $employee->roles()->detach();
        $employee->roles()->attach($request->jobTitle);
        $employee->save();
        $employee->contact->save();

        session()->flash('message', 'Usuário alterado com sucesso.');
        session()->flash('alert', 'success');
        
        return back();
    }

    public function destroy(Person $person)
    {
        if(\Auth::user()->person->roles->first()->id == 1)
        {
            if($person->status == 1)
                $msg = 'Usuário desativado com sucesso.';
            else
                $msg = 'Usuário ativado com sucesso.';

            session()->flash('message',$msg);
            session()->flash('alert','success');
            $person->changeStatus();
            return redirect()->action('EmployeeController@index');
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function search()
    {
        if(request('name') == null ) return back();

        if(request('column') == 'jobTitle')
        {
            $roles = Role::where(request('column'),'like','%'.request('name').'%')->first();

            if(empty($roles))
            {
                session()->flash('message','Não há usuário com esse cargo.');
                session()->flash('alert','info');
                return redirect()->action('EmployeeController@index');
            };

            if($roles->people->count() > 1)
            {
                $employees = $roles->people;
                
                foreach($employees as $e)
                {
                    $e->age();
                };

                session()->flash('result',$roles->people->count());
                return view('employee.index',compact('employees'));
            };

            $person = $roles->people->first();
            $person->age();
            return redirect()->action('EmployeeController@show',compact('person'));
        }

        $result = Person::with('roles')
                    ->where(request('column'),'like','%'.request('name').'%')
                    ->where('status',1)
                    ->whereExists(function ($query) {
                        $query->select(DB::raw(1))
                              ->from('person_role')
                              ->whereRaw('person_role.person_id = people.id');
                    })->orderBy('firstName', 'asc')->get();

        if($result->isEmpty())
        {
            session()->flash('message','Usuário não encontrado.');
            session()->flash('alert','info');
            return redirect()->action('EmployeeController@index');
        };

        if($result->count() > 1)
        {
            $employees = $result;
            
            foreach($employees as $e)
            {
                $e->age();
            };

            session()->flash('result',$result->count());
            return view('employee.index',compact('employees'));
        };
        $person = $result->first();
        $person->age();
        return redirect()->action('EmployeeController@show',compact('person'));
    }

    public function expedients(Person $person)
    {
        return Person::find($person->id)->expedients;
    }

    public function resetPassword(Request $request)
    {
        $validator = \Validator::make(
            ['password' => $request->password],
            ['password' => 'required']
        );

        if($validator->fails())
        {
            session()->flash('message',$validator->messages()->first());
            session()->flash('alert','danger');
            
        } else if($request->password != $request->password_confirmation){
            session()->flash('message','A confirmação de Senha não confere.');
            session()->flash('alert','danger');
        } else {
            session()->flash('message','Senha atualizada.');
            session()->flash('alert','success');

            $user = \Auth::user();
            $user->password = bcrypt($request->password);
            $user->save();
        }
        
        return redirect('employees/'.\Auth::user()->person->id);

    }
}