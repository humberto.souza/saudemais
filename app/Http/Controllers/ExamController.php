<?php

namespace Clinic\Http\Controllers;

use Clinic\Exam;
use Clinic\Agenda;
use Clinic\Person;
use Clinic\Http\Requests\StoreExam;
use Illuminate\Http\Request;

class ExamController extends Controller
{

    public function index(Person $person)
    {
        if(\Auth::user()->person->roles->first()->category_id == 5)
        {
            $exams = Agenda::all()
                    ->where('agendable_id','!=',null)
                    ->where('patient_id',$person->id)
                    ->where('agendable_type','Clinic\Exam')
                    ->where('doctor_id',\Auth::user()->person->id)
                    ->sortByDesc('date');

            if($exams->count() > 0)
                return view('exam.index', compact('exams'));
            else
            {
                session()->flash('message','Nenhum registro encontrado.');
                session()->flash('alert','info');
                return back();
            }

        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');
            return back();
        }
    }

    public function create()
    {
        if(session('agenda') == null) return redirect()->route('agenda');

        $agenda = session('agenda');
        
        $exams = explode("A", $agenda->exam);

        $type = \Clinic\Type::find($exams[0]);

        if( ($agenda->status == 'em atendimento') && ($agenda->doctor_id == \Auth::user()->person->id) )
        {
            $patient = $agenda->patient;
            $dt = \Carbon\Carbon::parse($patient->birthday);
            $patient->date = $dt->format('d/m/Y');
            $patient->age();

            return view('exam.create', compact('agenda', 'patient', 'type'));
        };
        
        return redirect('/agenda');
    }

    public function store(StoreExam $request)
    {
        $agenda = \Clinic\Agenda::find(request('agenda_id'));

        $exam = new Exam();

        $exam->observation = $request->observation;
        $exam->result = $request->result;

        if(collect(str_contains($agenda->exam, 'A'))->first())
        {
            $examExplode = explode("A",$agenda->exam);
            
            $exam->type()->associate(collect($examExplode)->first());
            $exam->appointment_id = collect($examExplode)->last();
        } else {
            $exam->type()->associate($agenda->exam);
        }

        $exam->save();

        $agenda->agendable()->associate($exam);
        $agenda->status = 'finalizado';
        
        $agenda->save();

        session()->flash('message','Exame cadastrado com sucesso.');
        session()->flash('alert','success');
        
        return redirect('/agenda/'.$agenda->id);
    }

    public function show(Exam $exam)
    {
        if( ($exam->agenda->first()->status == 'finalizado') && ($exam->agenda->first()->doctor_id == \Auth::user()->person->id ) )
        {
            $patient = $exam->agenda->first()->patient;
            $dt = \Carbon\Carbon::parse($patient->birthday);
            $patient->date = $dt->format('d/m/Y');
            $patient->age();

            return view('exam.show', compact('exam','patient'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');
            return back();
        }
    }

    public function edit(Exam $exam)
    {
        if(\Auth::user()->person->roles->first()->category_id != 5)
        {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');
            return back();
        };

        $dt = \Carbon\Carbon::parse($exam->agenda->first()->patient->birthday);
        $exam->agenda->first()->patient->date = $dt->format('d/m/Y');
        $exam->agenda->first()->patient->age();

        $exams = \Clinic\Type::all();
        
        return view('exam.edit',compact('exam','exams','requests'));
    }

    public function update(StoreExam $request, Exam $exam)
    {
        $exam->fill([
            'observation' => $request->observation,
            'result' => $request->result,
        ])->save();
        
        session()->flash('message','Edição realizada com sucesso.');
        session()->flash('alert','success');

        return redirect('/exams/'.$exam->id);
    }

    public function print(Exam $exam)
    {
        if(!collect($exam->appointment)->isEmpty())
        {

            if(\Auth::user()->person->id == $exam->appointment->agenda->first()->doctor->id || \Auth::user()->person->id == $exam->agenda->first()->doctor->id)
            {
                return view('exam.print',compact('exam'));
            
            } else {
                session()->flash('message','Usuário sem autorização');
                session()->flash('alert','danger');
                return redirect('/agenda');
            }

        } elseif(\Auth::user()->person->id == $exam->agenda->first()->doctor->id) {
            
            return view('exam.print',compact('exam'));
        } else {
            session()->flash('message','Usuário sem autorização');
            session()->flash('alert','danger');
            return redirect('/agenda');
        }
    }
}