<?php

namespace Clinic\Http\Controllers;

use Clinic\Agenda;
use Clinic\Http\Requests\StoreAgenda;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Clinic\Person;
use Clinic\Category;
use Clinic\Expedient;
use Clinic\Role;
use Clinic\Appointment;
use Clinic\Exam;
use Clinic\Medication;
use Clinic\Mail\ScheduleConfirmation;
use Clinic\Mail\ScheduleUpdate;
use Clinic\Mail\ScheduleCanceled;
use Illuminate\Support\Facades\DB;


class AgendaController extends Controller
{

    public function index()
    {
        if(\Auth::user()->person->roles->first()->id == 1)
            return redirect('/employees');
        /* 
            Recepcionist (Category - 2) See all schedules
            Doctors (Category 3 and 5) - Can see only they own schedules
        */
        $category = \Auth::user()->person->roles->first()->category_id;
        $employee = \Auth::user()->person->id;
        $doctors = Person::people('doctors');
        $patients = Person::patients('doctors');
        $doctors->each(function($d){
            $d->id = $d->person_id;
        });

        return view('agenda.index', compact('employee', 'category','doctors','patients'));
    }

    public function create()
    {
        if(\Auth::user()->person->roles->first()->id == 2)
        {
            $weekday = array('Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado');

            $patients = Person::patients();
            $roles = Category::find(3)->roles; //Category 3 --> Saúde
            $exams = \Clinic\Type::all();
            return view('agenda.create', compact('roles','patients','exams'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function store(StoreAgenda $request)
    {
        $type = \Clinic\Type::all();

        $agenda = new Agenda();
        $agenda->date = $request->date;
        $agenda->start = $request->start;
        $agenda->end = $agenda->setInterval();

        $agenda->agendable_type = $request->agendable_type;

        if(isset($request->exam))
            $agenda->exam = $request->exam;

        if(isset($request->appointment_id))
            $agenda->exam = $request->exam."A".$request->appointment_id;

        $patient = Person::find($request->patient_id);
        $doctor = Person::find($request->doctor_id);
        $scheduled = Person::find($request->created_by);
        
        $agenda->patient()->associate($patient);
        $agenda->doctor()->associate($doctor);
        $agenda->scheduled()->associate($scheduled);

        $agenda->save();

        session()->flash('message','Agendamento cadastrado com sucesso.');
        session()->flash('alert','success');

        $patient = Person::find($agenda->patient_id)->contact;
        \Mail::to($patient)->send(new ScheduleConfirmation($agenda,$type));

        return redirect()->action('AgendaController@index');
    }

    public function show(Agenda $agenda)
    {
        if(\Auth::user()->person->roles->first()->id == 1)
            return redirect('/employees');
        
        $agenda->count = Agenda::find($agenda->id)->patient->patient_schedule->where('status','finalizado')->where('doctor_id',$agenda->doctor_id)->count();
        $agenda->getFormatedAgendableType();
        $agenda->getFormatedHour();
        $agenda->created_date = $agenda->created_at->format('d/m/y');
        $agenda->created_time = $agenda->created_at->format('H:i');

        if(str_contains($agenda->exam,'A'))
        {
            $exams = explode("A", $agenda->exam);
            $type = \Clinic\Type::find($exams[0]);

        } else {
            $type = \Clinic\Type::find($agenda->exam);
        }

        return view('agenda.show',compact('agenda','type'));
    }

    public function edit(Agenda $agenda)
    {
        if(\Auth::user()->person->roles->first()->id <= 2)
        {
            if($agenda->status != 'marcado')
            {
                session()->flash('message','Não é possível editar o agendamento.');
                session()->flash('alert','danger');
                return back();
            }

            if(\Auth::user()->person->roles->first()->category_id != 2)
            {
                session()->flash('message','Usuário sem autorização.');
                session()->flash('alert','danger');
                return back();
            }

            $agenda->pdate = $agenda->getFormatedDate();
            $agenda->getFormatedHour();
            $agenda->getFormatedAgendableType();
            $agenda->patient->reducedName();
            $agenda->doctor->reducedName();
            $roles = Category::find(3)->roles;
            $exams = \Clinic\Type::all();
            $exam_type = \Clinic\Type::find($agenda->exam);
            $doctors = Role::find($agenda->doctor->roles->first()->id)->people->sortBy('firstName');

            $isDoctor = Person::people('doctors')->where('person_id',$agenda->patient->id)->count();

            if($isDoctor >= 1)
            {
                $patients = Person::people('doctors');
                $patients->each(function($p){
                    $p->id = $p->person_id;
                });
            }
            else
                $patients = Person::patients();

            return view('agenda.edit', compact('agenda','roles','exams','exam_type','doctors','patients','isDoctor'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function update(StoreAgenda $request, Agenda $agenda)
    {
        $patient = $agenda->patient->contact;
        $id = $agenda->id;
        $type = \Clinic\Type::all();

        $agenda->start = $request->start;
        $end = $agenda->setInterval();

        if(isset($request->exam))
        {
            $agenda = Agenda::find($agenda->id)
                ->fill([
                    'date' => $request->date,
                    'start' => $request->start,
                    'end' => $end,
                    'agendable_type' => $request->agendable_type,
                    'doctor_id' => $request->doctor_id,
                    'patient_id' => $request->patient_id,
                    'exam' => $request->exam,
                ])->save();

                session()->flash('message','Agendamento alterado com sucesso.');
                session()->flash('alert','success');
        } else {
            $agenda = Agenda::find($agenda->id)
                ->fill([
                    'date' => $request->date,
                    'start' => $request->start,
                    'end' => $end,
                    'agendable_type' => $request->agendable_type,
                    'doctor_id' => $request->doctor_id,
                    'patient_id' => $request->patient_id,
                    'exam' => null
                ])->save();

                session()->flash('message','Agendamento alterado com sucesso.');
                session()->flash('alert','success');
        }
        $a = Agenda::find($id);
        \Mail::to($patient)->send(new ScheduleUpdate($a,$type));

        return back();
    }

    public function destroy(Agenda $agenda)
    {
        if(\Auth::user()->person->roles->first()->id <= 2)
            {
            $agenda->status = 'cancelado';
            $agenda->save();

            session()->flash('message','Agendamento cancelado com sucesso.');
            session()->flash('alert','success');

            $type = \Clinic\Type::all();
            \Mail::to($agenda->patient->contact)->send(new ScheduleCanceled($agenda,$type));
            
            return redirect()->route('agenda');
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function changeStatus(Agenda $agenda)
    {
        $patient = $agenda->patient->patient_schedule;
        $patientBuzy = $patient->where('status','em atendimento')->count();

        $explode = explode("\\", $agenda->agendable_type);
        $pathUpper = $explode[1]; //appointment or exam
        $path = strtolower($pathUpper);

        if(request('status') == 'continuar atendimento')
        {
            return redirect('/'.$path.'s'.'/create')->with('agenda',$agenda);
        } else {

            if(request('status') == 'em atendimento' && $patientBuzy > 0)
            {
                session()->flash('message','O paciente não pode ser atendido pois está em outra consulta.');
                session()->flash('alert','danger');

                return back();
            }

            $agenda = Agenda::find($agenda->id);
            $agenda->status = request('status');
            $agenda->save();

            return redirect('/'.$path.'s'.'/create')->with('agenda',$agenda);
        }

    }

    public function weekday(Person $doctor)
    {
        return Expedient::all()->where('person_id',$doctor->id)->pluck('weekday');
    }

    public function doctorsByRole(Role $role)
    {
        if(request('doctorsFilter'))
            return Person::people('doctors')->where('person_id','!=',request('except'));
        if( !empty(request('except')) )
            return Person::doctorsByRole($role->id)->where('person_id','!=',request('except'));
        else
            return Person::doctorsByRole($role->id);
    }

    public function technicians($type)
    {
        if( !empty(request('except')) )
            return \Clinic\Type::find($type)->role->people->where('id','!=',request('except'));
        else
            return \Clinic\Type::find($type)->role->people;
    }

    public function schedules()
    {
        $recepcionist = \Auth::user()->person->roles->first()->category_id == 2;

        $agenda = Agenda::when(request('agendable'),function($query){
                    $query->where('agendable_type',request('agendable'));
                })
                ->when(request('status'),function($query){
                    $query->where('status',request('status'));
                })
                ->when(request('patient_id'),function($query){
                    $query->where('patient_id',request('patient_id'));
                })
                ->when(request('doctor_id') && $recepcionist,function($query){
                    $query->where('doctor_id',request('doctor_id'));
                })
                ->when(!request('status'),function($query){
                    $query->where('status','!=','cancelado');
                })                
                ->when(!$recepcionist,function($query){
                    $query->where('doctor_id', \Auth::user()->person->id);
                })->get();

        foreach ($agenda as $a)
        {
            $patient = $a->patient;
            $patient->reducedName();

            $a->title = $a->patient->firstName ." ". $patient->lastName;
            $a->url = '/agenda/'.$a->id;
            $a->start = $a->date ."T". $a->start;
            $a->end = $a->date ."T". $a->end;
            $a->color = $a->statusControl();
        }

        return $agenda;
    }

    public function doctorSchedules(Person $doctor)
    {
        $agenda = Agenda::where('doctor_id',$doctor->id)->where('status','!=','cancelado')->get();

        foreach ($agenda as $a)
        {
            $patient = $a->patient;
            $patient->reducedName();

            $a->title = $a->patient->firstName ." ". $patient->lastName;
            $a->url = '/agenda/'.$a->id;
            $a->start = $a->date ."T". $a->start;
            $a->end = $a->date ."T". $a->end;
            $a->color = $a->statusControl();
        }

        return $agenda;
    }

    public function patientType($type)
    {
        if($type == 'doctors')
            return Person::people('doctors');
        else 
            return Person::patients();
    }

    public function dateValidator()
    {
        $olderDate = 25; // This value means the date requested is lower than today

        if(request('date') < date('Y-m-d')) return $olderDate; 

        $agenda = DB::table('agendas')
            ->where('date',request('date'))
            ->where('doctor_id',request('doctor'))
            ->get();

        $agenda_id = request('agenda_id');
        
        if($agenda->isNotEmpty())
        {
            if(isset($agenda_id))
            {
                if($agenda->where('id',request('agenda_id'))->isNotEmpty())
                {
                    if(request('date') == date('Y-m-d') && $agenda->where('id',request('agenda_id'))->first()->id == request('agenda_id')) 
                    {
                        return 0;
                    }
                }
            }
        }

        return $agenda->count();
    }

    public function validatorForDoctorAsPatient()
    {
        $weekday = array('Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado');

        $patient = request('patient');
        $date = request('date');

        $tmRequestStart = request('start');
        $tmRequestEnd = date('H:i:s', strtotime('+15 minute', strtotime($tmRequestStart)));

        $dayOfWeek = Carbon::parse($date)->dayOfWeek;
        $e = Person::find($patient)->expedients->where('weekday',$dayOfWeek);

        $dayOfWeek = $weekday[$dayOfWeek];

        if($e->isNotEmpty()){
            $requestStart = Carbon::parse($tmRequestStart)->format('H:i');
            $requestEnd = Carbon::parse($tmRequestEnd)->format('H:i');

            $start = Carbon::parse($e->first()->start)->format('H:i');
            $end = Carbon::parse($e->first()->end)->format('H:i');

            $patient = Person::find($patient);
            $sex = $patient->gender == 'male' ? 'o' : 'a';

            if( ($requestEnd <= $start || $requestStart >= $end) )
                return ['true','Horário disponível.'];
            else
                return ['false','Às '. $dayOfWeek .'s, para '. $sex .' paciente '.$patient->firstName.' '. $patient->lastName .', só é possível agendar antes das '.$start.' e depois das '.$end];
        } else
            return ['true', 'O médico que está sendo agendado não possui expediente neste dia.'];
    }

    public function timeValidator()
    {
        /*Checking if the patient has another agenda with status "em atendimento" */
        $patientAgenda = DB::table('agendas')
                ->where('start',request('start'))
                ->where('date',request('date'))
                ->where('patient_id',request('patient'))
                ->where('status','!=','cancelado')
                ->get();

        $firstResult = $patientAgenda->count() == 0 ? 'true' : 'false';
        $firstMessage = $patientAgenda->count() == 0 ? 'Disponível' : 'O paciente já possui outro agendamento neste horário.';
        
        
        /*End of checking */
        if(request('date') == date('Y-m-d') && request('start') < date('H:i')) 
            return ['false','O horário escolhido não pode ser inferior ao horário atual.']; // This value, '2', means the time requested already passed

        $dayOfWeek = Carbon::parse(request('date'))->dayOfWeek;
        $e = Person::find(request('doctor'))->expedients->where('weekday',$dayOfWeek);
        
        if($e->isNotEmpty())
        {
            $start = $e->first()->start;
            $end = $e->first()->end;

            $requestStart = request('start');
            $requestEnd = date('H:i:s', strtotime('+15 minute', strtotime($requestStart)));
        } else {
            return ['false','Fora do horário de expediente, consulte a tabela de expediente.']; // Day out of range
        }

        

        if(request('start') >= $start && $requestEnd <= $end)
        {
            $agenda = DB::table('agendas')
                ->whereIn('start', [request('start')])
                ->where('date',request('date'))
                ->where('doctor_id',request('doctor'))
                ->where('status','!=','cancelado')
                ->get();
            
            //If's the same agenda_id it's possible to override the schedule
            $agenda_id = request('agenda_id');
            
            if($agenda->isNotEmpty())
            {
                if(isset($agenda_id))
                {
                    if($agenda->first()->id == request('agenda_id')) 
                    {
                        return ['true','Permitido, pois é o mesmo ID'];
                    }
                }
            }
            $result = $agenda->count() == 0 ? 'true' : 'false';
            $message = $agenda->count() == 0 ? 'Disponível' : 'Horário ocupado.';
            
            if($firstResult == 'false')
                return [$firstResult,$firstMessage];
            
            return [$result,$message];

        } else {
            return ['false','Fora do horário de expediente, consulte a tabela de expediente.']; // Time is out of range
        }
    }

    public function getTimeLimit()
    {
        $dayOfWeek = Carbon::parse(request('date'))->dayOfWeek;
        $e = Person::find(request('doctor'))->expedients->where('weekday',$dayOfWeek);
        $start = $e->first()->start;
        $end = $e->first()->end;

        return [$start,$end];
    }

    public function request($appointment, $request)
    {
        if(\Auth::user()->person->roles->first()->id <= 2)
        {
            $weekday = array('Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado');
            
            $appointment = Appointment::find($appointment);
            session()->flash('appointment', $appointment);
            
            $exam = $appointment->requests;
            $exams_array = explode(';',$exam);
            $exam = \Clinic\Type::where('type',$exams_array[$request-1])->first();

            return view('agenda.create',compact('exam','appointment'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }    
    }

    public function search()
    {
        if(request('date') == null && request('doctor_id') == null)
            return redirect('/patient/appointments/'.request('patient_id'));


        $appointments = Agenda::where('patient_id',request('patient_id'))
                ->when(request('date'),function($query){
                    $query->where('date',request('date'));
                })
                ->when(request('doctor_id'),function($query){
                    $query->where('doctor_id',request('doctor_id'));
                })->orderBy('date','desc')->orderBy('start','desc')->get();

        if($appointments->count() == 0)
        {
            session()->flash('message','Nenhum agendamento encontrado.');
            session()->flash('alert','info');
            return back();
        } else {
            $type = \Clinic\Type::all();
            $person = \Clinic\Person::find($appointments->first()->patient->id);
            $doctors = \Clinic\Person::people('doctors');
            $doctors->each(function($d){
                $d->id = $d->person_id;
            });
            $appointments->marcado = $appointments->count();
            $appointments->finalizado = $appointments->where('status','finalizado')->count();
            return view('appointment.index', compact('appointments','person','type','doctors','marcado','finalizado'));
        }
    }
}