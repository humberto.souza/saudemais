<?php

namespace Clinic\Http\Controllers;

use Clinic\Appointment;
use Clinic\Http\Requests\StoreAppointment;
use Clinic\Medication;
use Clinic\Agenda;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{

    public function index($person)
    {
        $doctors = \Clinic\Person::people('doctors');
        $doctors->each(function($d){
            $d->id = $d->person_id;
        });

        $appointments = Agenda::where('patient_id',$person)->orderBy('date','desc')->orderBy('start','desc')->get();
        $person = \Clinic\Person::find($person);

        $appointments->marcado = $appointments->count();
        $appointments->finalizado = Agenda::where('patient_id',$person->id)->where('status','finalizado')->count();

        if($appointments->isEmpty())
        {
            session()->flash('message','O paciente não possui histórico.');
            session()->flash('alert','info');
            return back();
        }

        if( \Auth::user()->person->roles->first()->category_id == 2 )
        {
            $type = \Clinic\Type::all();
            return view('appointment.index', compact('appointments','person','type','doctors'));
        } else {
            return back();
        }
    }

    public function create()
    {
        if(session('agenda') == null) return redirect()->route('agenda');

        $agenda = session('agenda');

        if( ($agenda->status == 'em atendimento') && ($agenda->doctor_id == \Auth::user()->person->id) )
        {
            $patient = $agenda->patient;
            $dt = Carbon::parse($patient->birthday);
            $patient->date = $dt->format('d/m/Y');
            $patient->age();

            $exams = \Clinic\Type::all();

            return view('appointment.create', compact('agenda', 'patient','exams'));
        };
        
        return redirect('/agenda');
    }

    public function store(StoreAppointment $request)
    {
        $medications = collect($request->medication_id);
        $prescriptions = collect($request->prescription);

        $exams = $request->exams;
        $requests = "";

        if($request->exams != null)
        {
            foreach ($exams as $e)
            {
                $requests = $requests.";".$e;
            };
        };

        $agenda = Agenda::find(request('agenda_id'));
        $app = Appointment::create($request->all());
        $app->addPrescription($medications,$prescriptions);
        
        $agenda->agendable()->associate($app);
        
        $agenda->status = 'finalizado';
        $app->requests = $requests;
        
        $app->save();
        $agenda->save();

        session()->flash('message','Consulta encerrada com sucesso.');
        session()->flash('alert','success');

        return redirect('/agenda/'.$agenda->id);
    }

    public function show(Appointment $appointment)
    {
        if( ($appointment->agenda->first()->status == 'finalizado') && ($appointment->agenda->first()->doctor_id == \Auth::user()->person->id || \Auth::user()->person->roles->first()->category_id == 2) )
        {
            $requests = explode(';',$appointment->requests);
            $appointment->requests = $requests;

            $patient = $appointment->agenda->first()->patient;
            $dt = Carbon::parse($patient->birthday);
            $patient->date = $dt->format('d/m/Y');
            $patient->age();

            return view('appointment.show', compact('appointment','patient'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');
            return back();
        }
    }

    public function edit(Appointment $appointment)
    {
        
        if(\Auth::user()->person->roles->first()->category_id != 3)
        {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');
            return back();
        };

        $dt = Carbon::parse($appointment->agenda->first()->patient->birthday);
        $appointment->agenda->first()->patient->date = $dt->format('d/m/Y');
        $appointment->agenda->first()->patient->age();

        $requests = collect(explode(";",$appointment->requests));
        $appointment->requests = $requests;

        $exams = \Clinic\Type::all();
        
        return view('appointment.edit',compact('appointment','exams','requests'));
    }

    public function update(StoreAppointment $request, Appointment $appointment)
    {
        $canceled_medications = collect($request->canceled_medication_id);

        $existing_medications = collect($request->existing_medication_id);
        $existing_prescriptions = collect($request->existing_prescription);

        $medications = collect($request->medication_id);
        $prescriptions = collect($request->prescription);

        $appointment->addPrescription($medications,$prescriptions);

        foreach ($canceled_medications as $i => $canceled) {
            $appointment->medications()->detach($canceled);
        };

        foreach ($existing_medications as $i => $existing) {
            $appointment->medications()->updateExistingPivot($existing, ['prescription' => $existing_prescriptions[$i]]);
        };

        $exams = $request->exams;
        $requests = "";

        if($request->exams != null)
        {
            foreach ($exams as $e)
            {
                $requests = $requests.";".$e;
            };
        };

        $request->requests = $requests;

        $appointment->fill([
            'symptoms' => $request->symptoms,
            'prescription' => $request->prescription,
            'requests' => $request->requests
        ])->save();
        
        session()->flash('message','Edição realizada com sucesso.');
        session()->flash('alert','success');


        return redirect('/appointments/'.$appointment->id);
    }

    public function medications()
    {
        return Medication::all()->where('type',request('type'));
    }

    public function history(Agenda $agenda)
    {
        if($agenda->doctor_id != \Auth::user()->person->id)
        {
            return redirect()->route('agenda');
        }
        $patient = $agenda->patient;

        $history = Agenda::where('patient_id',$patient->id)
            ->where('status','finalizado')
            ->where('doctor_id',$agenda->doctor_id)
            ->orderBy('date','desc')
            ->orderBy('start','desc')
            ->get();

        if($history->isEmpty()) 
        {
            session()->flash('message','Nenhum histórico encontrado.');
            session()->flash('alert','info');
            return back();
        };

        foreach ($history as $h) {
            $requests = $h->agendable->requests;
            $h->requests = explode(";",$requests);
        };

        return view('appointment.history',compact('history'));
    }

    public function print($type)
    {
        $url = explode('/',url()->previous());
        $agendaId = array_pop($url);

        $prescriptions = Agenda::find($agendaId)->agendable->medications;

        $value = Agenda::find($agendaId)->agendable->requests;

        $requests = explode(';',$value);
        unset($requests[0]);

        $doctor = Agenda::find($agendaId)->doctor;
        $patient = Agenda::find($agendaId)->patient;
        $patient->age();

        return view('appointment.print', compact('prescriptions','patient','doctor','requests','type'));
    }

}