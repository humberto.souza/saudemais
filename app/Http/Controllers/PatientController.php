<?php

namespace Clinic\Http\Controllers;

use Clinic\Person;
use Clinic\Http\Requests\StorePatient;
use Clinic\Http\Requests\UpdatePatient;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class PatientController extends Controller
{

    public function index()
    {
        $patients = Person::people('patients');
        return view('patient.index',compact('patients'));
    }

    public function create()
    {
        if(\Auth::user()->person->roles->first()->id <= 2)
        {
            return view('patient.create');
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function store(StorePatient $request)
    {
        Person::create($request->all())->addContact($request);

        session()->flash('message', 'Paciente cadastrado com sucesso.');
        session()->flash('alert', 'success');

        return redirect()->action('PatientController@index');
    }

    public function show(Person $person)
    {   
        if($person->status == 0 && \Auth::user()->person->roles->first()->category_id != 1)
        {
            session()->flash('message','Usuário não encontrado.');
            session()->flash('alert','info');
            return back();
        }

        $person->schedule = $person->patient_schedule->sortByDesc('date');
        $person->age();
        return view('patient.show',compact('person'));
    }

    public function edit(Person $person)
    {
        if(\Auth::user()->person->roles->first()->id <= 2)
        {
            return view('patient.edit',compact('person'));
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function update(UpdatePatient $request, Person $person)
    {
        if(!$person->emailCheck($request->email,$person))
        {
            session()->flash('message', 'E-mail já está em uso.');
            session()->flash('alert', 'danger');
        
            return back();
        }

        /*if(!$person->cpfCheck($request->cpf))
        {
            session()->flash('message', 'CPF já está em uso.');
            session()->flash('alert', 'danger');
        
            return back();
        }*/

        if(!$person->rgCheck($request->rg,$person))
        {
            session()->flash('message', 'RG já está em uso.');
            session()->flash('alert', 'danger');
        
            return back();
        }
        
        session()->flash('message', 'Paciente alterado com sucesso.');
        session()->flash('alert', 'success');
        $patient = Person::find($person->id)->fill($request->all());
        $patient->contact->fill($request->all())->save();
        $patient->save();
        return back();
    }

    public function destroy(Person $person)
    {
        if(\Auth::user()->person->roles->first()->category_id <= 2)
        {
            if($person->status == 1)
                $msg = 'Paciente desativado com sucesso.';
            else
                $msg = 'Paciente ativado com sucesso.';

            session()->flash('message',$msg);
            session()->flash('alert','success');
            $person->changeStatus();
            return redirect()->action('PatientController@index');
        } else {
            session()->flash('message','Usuário sem autorização.');
            session()->flash('alert','danger');

            return back();
        }
    }

    public function search()
    {
        if(request('name') == null ) return back();

        if(\Auth::user()->person->roles->first()->category_id == 1)
        {
            $result = Person::with('roles')
                        ->where(request('column'),'like','%'.request('name').'%')
                        ->whereNotExists(function ($query) {
                            $query->select(DB::raw(1))
                                  ->from('person_role')
                                  ->whereRaw('person_role.person_id = people.id');
                        })->orderBy('firstName', 'asc')->get();
        } else {
            $result = Person::with('roles')
                    ->where(request('column'),'like','%'.request('name').'%')
                    ->where('status',1)
                    ->whereNotExists(function ($query) {
                        $query->select(DB::raw(1))
                              ->from('person_role')
                              ->whereRaw('person_role.person_id = people.id');
                    })->orderBy('firstName', 'asc')->get();
        };
        
        if($result->isEmpty())
        {
            session()->flash('message','Paciente não encontrado.');
            session()->flash('alert','info');
            return redirect()->action('PatientController@index');
        };

        if($result->count() > 1)
        {
            $patients = $result;
            
            foreach($patients as $p)
            {
                $p->age();
            };

            session()->flash('result',$result->count());
            return view('patient.index',compact('patients'));
        };
        $person = $result->first();
        $person->age();
        return view('patient.show',compact('person'));
    }
}