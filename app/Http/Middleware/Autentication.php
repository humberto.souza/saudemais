<?php

namespace Clinic\Http\Middleware;

use Closure;

class Autentication
{

    public function handle($request, Closure $next)
    {
    	if(!\Auth::guest() && \Auth::user()->person->status == 0)
    	{
    		\Auth::logout();
			return redirect()->guest('login')
				->with(
					[
						'message' =>'Usuário desativado entre em contato com o administrador.',
						'alert' => 'danger'
					]);
    	}

        if(!$request->is('login') && \Auth::guest())
        {
            return redirect('/login');
        }
        return $next($request);
    }
}
