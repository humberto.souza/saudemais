<?php

namespace Clinic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePatient extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'firstName' => 'required|min:3',
            'lastName' => 'required|min:3',
            'birthday' => 'required|date',
            'gender' => 'required',
            'cpf' => 'required|numeric|unique:people',
            'rg' => 'required|numeric|unique:people',
            'streetName' => 'required',
            'number' => 'required',
            'address' => 'required|max:255',
            'postcode' => 'required|numeric',
            'email' => 'required|unique:contacts',
            'phone' => 'required'
        ];
    }
}
