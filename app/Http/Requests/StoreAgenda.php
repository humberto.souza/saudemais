<?php

namespace Clinic\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAgenda extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'agendable_type' => 'required',
            'patient_id' => 'required',
            'doctor_id' => 'required',
            'date' => 'required|date',
            'start' => 'required'
        ];
    }
}
