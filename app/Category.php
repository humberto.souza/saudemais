<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function roles()
    {
        return $this->hasMany(Role::class);
    }
}
