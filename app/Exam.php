<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $fillable = ['result', 'observation','justification'];

	public function type()
	{
		return $this->belongsTo(Type::class);
	}

    public function agenda()
    {
        return $this->morphMany(Agenda::class, 'agendable');
    }

    public function appointment()
    {
    	return $this->belongsTo(Appointment::class);
    }
}
