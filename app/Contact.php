<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
	protected $fillable = ['email', 'phone', 'landline'];
	
	public static function associate(Person $person)
	{
		$contact = new Contact();
		$contact->email = request('email');
		$contact->phone = request('phone');
		$contact->landline = request('landline');
		$contact->person()->associate($person);
		$contact->save();
	}

    public function person()
    {
        return $this->belongsTo(Person::Class);
    }
}
