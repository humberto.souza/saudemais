<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function exams()
    {
    	return $this->hasMany(Exam::class);
    }

    public function role()
    {
    	return $this->belongsTo(Role::class);
    }
}
