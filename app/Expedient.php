<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Expedient extends Model
{
	protected $fillable = ['weekday','start','end'];
	
    public function person()
    {
        return $this->belongsTo(Person::Class);
    }

    public function agendas()
    {
        return $this->hasMany(Agenda::class);
    }
}
