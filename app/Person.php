<?php

namespace Clinic;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Person extends Model
{
    protected $fillable = ['firstName','lastName','birthday','cpf','rg','gender','streetName','number','address','secondaryAddress','postcode'];

    public function emailCheck($email,$person)
    {
        if($person->contact->email == $email)
            return true;
        else {
            $validator = \Clinic\Contact::where('email',$email)->count();

            if($validator > 0)
                return false;
            else 
                return true;
        }
    }

    public function cpfCheck($cpf,$person)
    {
        if($person->cpf == $cpf)
            return true;
        else {
            $validator = Person::where('cpf',$cpf)->count();

            if($validator > 0)
                return false;
            else 
                return true;
        }
    }

    public function rgCheck($rg,$person)
    {
        if($person->rg == $rg)
            return true;
        else {
            $validator = Person::where('rg',$rg)->count();

            if($validator > 0)
                return false;
            else 
                return true;
        }
    }

    public function addContact($request)
    {
        $this->contact()->create($request->all());
    }

    public function addRole($request)
    {
        $this->roles()->attach($request->jobTitle);
    }

    public function addExpedient($request)
    {
        for($i = 0; $i < sizeof($request->weekday); $i++) {
            $this->expedients()->create([
                'weekday' => $request->weekday[$i],
                'start' => $request->start[$i],
                'end' => $request->end[$i]
            ]);
        }
    }

    // returns a collection with employees and patients
    public static function people($type)
    {
        $people = Person::peopleQuery($type);

        foreach ($people as $p) {
            
            $p->age();

            $p->reducedName();

        }
        return $people;        
    }

    public static function doctorsByRole($roles)
    {
        Role::find($roles)->category_id == 3 ? $category = 3 : $category = 5;
        
        return $people = Person::with('roles')
                ->join('person_role', 'people.id', '=', 'person_role.person_id')
                ->join('roles', 'roles.id', '=', 'person_role.role_id')
                ->where('people.status','=',1)
                ->where('roles.id','=',$roles)
                ->where('roles.category_id','=',$category)
                ->orderBy('firstName', 'asc')
                ->get();

        foreach ($people as $p) {
            
            $p->age();

            $p->reducedName();

        }
        return $people;

    }

    public static function patients()
    {
        $people = Person::with('roles')
        ->where('status','=',1)
        ->whereNotExists(function ($query) {
            $query->select(DB::raw(1))
                  ->from('person_role')
                  ->whereRaw('person_role.person_id = people.id');
        })->orderBy('firstName', 'asc')
        ->get();

        foreach ($people as $p) {
            
            $p->age();

            $p->reducedName();

        }
        return $people;

    }

    public static function peopleQuery($type)
    {
         if($type == "patients" && \Auth::user()->person->roles->first()->category_id == 1) {
            return Person::with('roles')
                ->orderBy('firstName', 'asc')
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('person_role')
                          ->whereRaw('person_role.person_id = people.id');
                })
                ->paginate(5);
        }else if($type == "patients") {
            return Person::with('roles')
                ->orderBy('firstName', 'asc')
                ->where('status','=',1)
                ->whereNotExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('person_role')
                          ->whereRaw('person_role.person_id = people.id');
                })
                ->paginate(5);
        } else if($type == "employees" && \Auth::user()->person->roles->first()->category_id == 1) {
            return Person::with('roles')
                ->orderBy('firstName', 'asc')
                ->where('id','>',1)
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('person_role')
                          ->whereRaw('person_role.person_id = people.id');
                })
                ->paginate(5);
        } else if($type == "employees") {
            return Person::with('roles')
                ->orderBy('firstName', 'asc')
                ->where('status','=',1)
                ->where('id','>',1)
                ->whereExists(function ($query) {
                    $query->select(DB::raw(1))
                          ->from('person_role')
                          ->whereRaw('person_role.person_id = people.id');
                })
                ->paginate(5);
        } else if($type == "doctors") {
            return Person::with('roles')
                ->join('person_role', 'people.id', '=', 'person_role.person_id')
                ->join('roles', 'roles.id', '=', 'person_role.role_id')
                ->where('people.status','=',1)
                ->where('roles.category_id','=',3)
                ->orWhere('roles.category_id','=',5)
                ->orderBy('firstName', 'asc')
                ->get();

        } else if ($type == "recepcionist") {
            return Person::with('roles')
                ->join('person_role', 'people.id', '=', 'person_role.person_id')
                ->join('roles', 'roles.id', '=', 'person_role.role_id')
                ->where('people.status','=',1)
                ->where('roles.category_id','=',2)
                ->orderBy('firstName', 'asc')
                ->get();
        }
    }

    public function changeStatus()
    {
        $status = $this->status == true ? false : true;
        $this->status = $status;
        $this->save();
    }

    // formats the birthday property with the string below
    public function age()
    {
        $dt = Carbon::parse($this->birthday);

        $y = $dt->year;
        $m = $dt->month;
        $d = $dt->day;
        
        $dt = Carbon::createFromDate($y, $m, $d)->diff(Carbon::now());

        $year = $dt->y == 1 ? 'ano' : 'anos';

        $month = $dt->m == 1 ? 'mês' : 'meses';

        $day = $dt->d == 1 ? 'dia' : 'dias';
        
        $this->birthday = $dt->format("%y ". $year .", %m ". $month ." e %d ". $day .".");
    }

    // divides the last name with, explode's method, and returns the last position
    public function reducedName()
    {
        $last = explode(" ", $this->lastName);
        return $this->lastName = $last[sizeof($last)-1];
    }

    public function roles()
    {
        return $this->belongsToMany(Role::Class);
    }

    public function contact()
    {
        return $this->hasOne(Contact::Class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function expedients()
    {
        return $this->hasMany(Expedient::class);
    }

    public function patient_schedule()
    {
        return $this->hasMany(Agenda::class,'patient_id');
    }

    public function doctor_schedule()
    {
        return $this->hasMany(Agenda::class,'doctor_id');
    }

    public function created_by()
    {
        return $this->hasMany(Agenda::class,'created_by');
    }
}
