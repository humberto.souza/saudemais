<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Agenda extends Model
{
    protected $fillable = [
        'date',
        'start',
        'end',
        'agendable_type',
        'exam',
        'doctor_id',
        'patient_id',
        'created_by',
    ];

    public function statusControl()
    {
        if($this->status == 'marcado'){
            
            if($this->agendable_type == 'Clinic\Exam'){
                return "#6A5ACD";
            }

            return "#5cb85c"; //verde

        } else if($this->status == 'aguardando') {
            return "#f0ad4e"; //amarelo

        } else if($this->status == 'em atendimento') {
            //default

        } else if($this->status == 'finalizado' || $this->status == 'cancelado' || $this->status == 'invalidado') {
            return "#d9534f"; //vermelho

        } else if($this->status == 'faltou') {
            return "#6C7B8B"; //cinza

        }

    }

    public function getFormatedDate()
    {
        $dt = Carbon::parse($this->date);

        //alterar o formato para "Sexta-feira, dia 22 de Fevereiro."
        return $dt->format('d/m/y');
    }

    public function getFormatedHour()
    {
        $dt = Carbon::parse($this->start);
        return $this->start = $dt->format('H:i');
    }
    
    public function setInterval()
    {
        //This function will add the interval to finish the appointment or exam.
        return date('H:i', strtotime('+15 minute', strtotime($this->start)));
    }

    public function getFormatedAgendableType()
    {
        if ( $this->agendable_type == 'Clinic\Appointment')
        {
            $this->type = 'Consulta';
        } else {
            $this->type = 'Exame';
        }
    }

    public function agendable()
    {
        return $this->morphTo();
    }

    public function patient()
    {
        return $this->belongsTo(Person::class,'patient_id');
    }

    public function doctor()
    {
        return $this->belongsTo(Person::class,'doctor_id');
    }

    public function scheduled()
    {
        return $this->belongsTo(Person::class,'created_by');
    }
}
