<?php

namespace Clinic;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
	protected $fillable = ['symptoms','requests'];

    public function addPrescription($medications,$prescriptions)
    {
        foreach ($medications as $i => $medication) {
            $this->medications()->attach($medication, ['prescription' => $prescriptions[$i] ]);
        }
    }

    public function agenda()
    {
        return $this->morphMany(Agenda::class, 'agendable');
    }

    public function exams()
    {
    	return $this->hasMany(Exam::class);
    }

    public function medications()
    {
    	return $this->belongsToMany(Medication::class)->withPivot('prescription');
    }
}
