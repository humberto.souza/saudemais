<?php

namespace Clinic\Mail;

use Clinic\Agenda;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScheduleUpdate extends Mailable
{
    use Queueable, SerializesModels;
    public $agenda;
    public $type;

    public function __construct(Agenda $agenda, $type)
    {
        $this->agenda = $agenda;
        $this->type = $type;
    }

    public function build()
    {
        return $this->view('emails.edit')->subject('Alteração no agendamento');
    }
}
