<?php

namespace Clinic\Mail;

use Clinic\Agenda;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ScheduleConfirmation extends Mailable
{
    use Queueable, SerializesModels;
    public $agenda;
    public $type;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Agenda $agenda, $type)
    {
        $this->agenda = $agenda;
        $this->type = $type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.create')->subject('Confirmação de agendamento');
    }
}