<?php

use Illuminate\Database\Seeder;
use Clinic\Type;
class TypeTableSeeder extends Seeder
{

    public function run()
    {
        Type::create(['type' => 'RX Articulação Esterno Clavicular','role_id' => 10]);
        Type::create(['type' => 'RX Articulação Sacro Iliaca','role_id' => 10]);
        Type::create(['type' => 'RX Bacia','role_id' => 10]);
        Type::create(['type' => 'RX Braço','role_id' => 10]);
        Type::create(['type' => 'RX Calcaneo','role_id' => 10]);
        Type::create(['type' => 'RX Cavum Lat Hirtz','role_id' => 10]);
        Type::create(['type' => 'RX Charneira','role_id' => 10]);
        Type::create(['type' => 'RX Clavicula','role_id' => 10]);
        Type::create(['type' => 'RX Coluna Cervical','role_id' => 10]);
        Type::create(['type' => 'RX Coluna Dorsal','role_id' => 10]);
        Type::create(['type' => 'RX Coluna Lombo Sacra','role_id' => 10]);
        Type::create(['type' => 'RX Coluna para Escoliose','role_id' => 10]);
        Type::create(['type' => 'US Obstetrica','role_id' => 13]);
        Type::create(['type' => 'US Obstetrica Gemelar','role_id' => 13]);
        Type::create(['type' => 'US Obstetrico C/ Doppler','role_id' => 13]);
        Type::create(['type' => 'US Ombro','role_id' => 13]);
        Type::create(['type' => 'US Parede Abdominal','role_id' => 13]);
        Type::create(['type' => 'US Panturrilha','role_id' => 13]);
        Type::create(['type' => 'US Pé','role_id' => 13]);
        Type::create(['type' => 'US Pelvica','role_id' => 13]);
        Type::create(['type' => 'US Pelvica Transvaginal','role_id' => 13]);
        Type::create(['type' => 'US Perna','role_id' => 13]);
        Type::create(['type' => 'US Punho','role_id' => 13]);
        Type::create(['type' => 'US Quadril','role_id' => 13]);
        Type::create(['type' => 'Ecocardiograma com Doppler','role_id' => 12]);
        Type::create(['type' => 'Eletrocardiograma','role_id' => 12]);
        Type::create(['type' => 'Endoscopia Digestiva Alta','role_id' => 12]);
        Type::create(['type' => 'Holter','role_id' => 12]);
        Type::create(['type' => 'Mamografia','role_id' => 11]);
        Type::create(['type' => 'Mamografia Com Magnificação','role_id' => 11]);
        Type::create(['type' => 'Mapa','role_id' => 12]);
    }
}
