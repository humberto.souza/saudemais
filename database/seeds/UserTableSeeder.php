<?php

use Illuminate\Database\Seeder;
use Clinic\User;

class UserTableSeeder extends Seeder
{
    
    public function run()
    {
        User::create([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin'),
            'person_id' => 1
        ]);
    }
}
