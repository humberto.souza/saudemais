<?php

use Illuminate\Database\Seeder;
use Clinic\Medication;

class MedicationTableSeeder extends Seeder
{

    public function run()
    {
        Medication::create([
        	'name' => 'Dorflex',
        	'factoryName' => 'Dipirona Monoidratada',
        	'manufacturer' => 'Sanofi-Aventis',
        	'type' => 'Analgésico'
        ]);
        Medication::create([
        	'name' => 'Cyfenol',
        	'factoryName' => 'Paracetamol',
        	'manufacturer' => 'Cifarma',
        	'type' => 'Analgésico'
        ]);
        Medication::create([
        	'name' => 'Dorsanol',
        	'factoryName' => 'Paracetamol',
        	'manufacturer' => 'Multilab',
        	'type' => 'Analgésico'
        ]);
        Medication::create([
            'name' => 'Neosulida',
            'factoryName' => 'Nimesulida',
            'manufacturer' => 'Neo Química',
            'type' => 'Antiinflamatório'
        ]);
        Medication::create([
            'name' => 'Nimesubal',
            'factoryName' => 'Nimesulida',
            'manufacturer' => 'Baldacci',
            'type' => 'Antiinflamatório'
        ]);
        Medication::create([
            'name' => 'Arflex-Retard',
            'factoryName' => 'Nimesulida',
            'manufacturer' => 'Diffucap Chemobras',
            'type' => 'Antiinflamatório'
        ]);
        Medication::create([
            'name' => 'Allegra',
            'factoryName' => 'Cloridrato De Fexofenadina',
            'manufacturer' => 'Sanofi-Aventis',
            'type' => 'Antialérgico'
        ]);
        Medication::create([
            'name' => 'Prednisona',
            'factoryName' => 'Prednisona',
            'manufacturer' => 'Medley',
            'type' => 'Antialérgico'
        ]);
        Medication::create([
            'name' => 'Clarilerg',
            'factoryName' => 'Loratadina',
            'manufacturer' => 'Sandoz',
            'type' => 'Antialérgico'
        ]);
        Medication::create([
            'name' => 'Amoxicilina',
            'factoryName' => 'Amoxicilina',
            'manufacturer' => 'Medley',
            'type' => 'Antibiótico'
        ]);
        Medication::create([
            'name' => 'Amoxil',
            'factoryName' => 'Amoxicilina',
            'manufacturer' => 'GSK',
            'type' => 'Antibiótico'
        ]);
        Medication::create([
            'name' => 'Velamox',
            'factoryName' => 'Amoxicilina',
            'manufacturer' => 'EMS',
            'type' => 'Antibiótico'
        ]);
    }
}
