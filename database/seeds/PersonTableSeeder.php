<?php

use Illuminate\Database\Seeder;
use Clinic\Person;

class PersonTableSeeder extends Seeder
{

    public function run()
    {
        Person::create([
            'firstName' => 'Administrador',
            'lastName' => 'TI',
            'birthday' => '1992-09-21',
            'gender' => 'male',
            'cpf' => rand(11111111111, 99999999999),
            'rg' => rand(11111111, 99999999),
            'streetName' => 'Rua Filipinas',
            'number' => '250',
            'address' => 'Zumbi dos Palmares',
            'postcode' => rand(11111111, 55555555)
        ]);
        
        factory("Clinic\Person", 50)->create()->each(function ($p) {

            if($p->id <= 28)
            {
                $p->contact()->save(factory("Clinic\Contact")->make());
                $p->user()->save(factory("Clinic\User")->make());

                $p->user->email = strtolower($p->firstName) . "@gmail.com";
                
                $p->contact->email = $p->user->email;
                $p->user->password = bcrypt('saudemais');
                
                $p->user->save();
                $p->contact->save();

                if($p->id > 2)
                {
                    for($i = 1;$i <= 5; $i++){
                        $expedients = factory("Clinic\Expedient")->make();
                        $p->expedients()->create([
                            "weekday" => $i,
                            "start" => $expedients->start,
                            "end" => $expedients->end
                        ]);
                    }
                }

            }else{
                $p->contact()->save(factory("Clinic\Contact")->make());
                $p->contact->email = strtolower($p->firstName) . "@gmail.com";
                $p->contact->save();
            };
        });
    }
}