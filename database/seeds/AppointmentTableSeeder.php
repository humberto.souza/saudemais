<?php

use Illuminate\Database\Seeder;
use Clinic\Appointment;

class AppointmentTableSeeder extends Seeder
{

    public function run()
    {
        Appointment::create([
            'diagnostic' => 'Dor de cabeça',
            'remedy' => 'Anador',
            'treatment' => 'Tomar o medicamento de 12 em 12 horas.',
            'agenda_id' => 1
        ]);

        Appointment::create([
            'diagnostic' => 'Congestionamento nazal',
            'remedy' => 'Anador',
            'treatment' => 'Anador quando necessário e evitar ambientes empoeirados.',
            'agenda_id' => 2
        ]);
    }
}
