<?php

use Illuminate\Database\Seeder;
use Clinic\Exam;

class ExamTableSeeder extends Seeder
{

    public function run()
    {
        Exam::create(['type' => 'Tomografia','results' => 'arquivo.doc', 'agenda_id' => '3']);
    }
}
