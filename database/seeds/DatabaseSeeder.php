<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(PersonTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(ContactTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(Person_RoleTableSeeder::class);
        //$this->call(AgendaTableSeeder::class);
        //$this->call(ExamTableSeeder::class);
        //$this->call(AppointmentTableSeeder::class);
        $this->call(MedicationTableSeeder::class);
        $this->call(TypeTableSeeder::class);
    }
}