<?php

use Illuminate\Database\Seeder;
use Clinic\Agenda;

class AgendaTableSeeder extends Seeder
{

    public function run()
    {
       factory('Clinic\Agenda',20)->create();
    }
}
