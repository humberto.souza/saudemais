<?php

use Illuminate\Database\Seeder;
use Clinic\Category;

class CategoryTableSeeder extends Seeder
{

    public function run()
    {
        Category::create(['name' => 'Gestão']);
        Category::create(['name' => 'Administração']);
        Category::create(['name' => 'Saúde']);
        Category::create(['name' => 'Informática']);
        Category::create(['name' => 'Exames']);
    }
}
