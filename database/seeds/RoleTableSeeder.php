<?php

use Illuminate\Database\Seeder;
use Clinic\Role;
use Clinic\Person;

class RoleTableSeeder extends Seeder
{

    public function run()
    {
        Role::create(['jobTitle' => 'Administrador', 'category_id' => 1]);
        Role::create(['jobTitle' => 'Recepcionista', 'category_id' => 2]);
        Role::create(['jobTitle' => 'Cardiologista', 'category_id' => 3]);
        Role::create(['jobTitle' => 'Clínico Geral', 'category_id' => 3]);
        Role::create(['jobTitle' => 'Gastroenterologista', 'category_id' => 3]);
        Role::create(['jobTitle' => 'Ginecologista', 'category_id' => 3]);
        Role::create(['jobTitle' => 'Neurologista', 'category_id' => 3]);
        Role::create(['jobTitle' => 'Oftalmologista', 'category_id' => 3]);
        Role::create(['jobTitle' => 'Ortopedista', 'category_id' => 3]);
        Role::create(['jobTitle' => 'Técnico em Radiologia', 'category_id' => 5]);
        Role::create(['jobTitle' => 'Técnico em Mamografia', 'category_id' => 5]);
        Role::create(['jobTitle' => 'Técnico Geral', 'category_id' => 5]);
        Role::create(['jobTitle' => 'Técnico de Ultrassom', 'category_id' => 5]);
        Role::create(['jobTitle' => 'Técnico de Ressonância Magnética', 'category_id' => 5]);
    }
}

class Person_RoleTableSeeder extends Seeder 
{

    public function run()
    {
        $employee = Clinic\Person::find(1);
        $role = Clinic\Role::find(1);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(2);
        $role = Clinic\Role::find(2);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(3);
        $role = Clinic\Role::find(2);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(4);
        $role = Clinic\Role::find(3);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(5);
        $role = Clinic\Role::find(3);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(6);
        $role = Clinic\Role::find(4);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(7);
        $role = Clinic\Role::find(4);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(8);
        $role = Clinic\Role::find(5);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(9);
        $role = Clinic\Role::find(5);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(10);
        $role = Clinic\Role::find(6);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(11);
        $role = Clinic\Role::find(6);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(12);
        $role = Clinic\Role::find(7);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(13);
        $role = Clinic\Role::find(7);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(14);
        $role = Clinic\Role::find(8);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(15);
        $role = Clinic\Role::find(8);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(16);
        $role = Clinic\Role::find(9);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(18);
        $role = Clinic\Role::find(9);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(19);
        $role = Clinic\Role::find(10);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(20);
        $role = Clinic\Role::find(10);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(21);
        $role = Clinic\Role::find(11);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(22);
        $role = Clinic\Role::find(11);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(23);
        $role = Clinic\Role::find(12);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(24);
        $role = Clinic\Role::find(12);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(25);
        $role = Clinic\Role::find(13);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(26);
        $role = Clinic\Role::find(13);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(27);
        $role = Clinic\Role::find(14);
        $employee->roles()->attach($role);

        $employee = Clinic\Person::find(28);
        $role = Clinic\Role::find(14);
        $employee->roles()->attach($role);
    }
}