<?php

use Illuminate\Database\Seeder;
use Clinic\Contact;
use Clinic\Person;

class ContactTableSeeder extends Seeder
{

    public function run()
    {
        Contact::create([
            'phone' => rand(111111111, 999999999),
            'email' => 'admin@gmail.com',
            'person_id' => '1'
        ]);
    }
}