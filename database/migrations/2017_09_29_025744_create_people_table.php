<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{

    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->date('birthday');
            $table->enum('gender',['male','female']);
            $table->char('cpf',14);
            $table->string('rg');
            $table->string('streetName');
            $table->string('number');
            $table->string('address');
            $table->string('secondaryAddress')->nullable();
            $table->char('postcode',9);
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('people');
    }
}
