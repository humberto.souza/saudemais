<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAgendasTableAddForeignKeys extends Migration
{

    public function up()
    {
        Schema::table('agendas', function (Blueprint $table) {
            $table->integer('doctor_id')->unsigned();
            $table->foreign('doctor_id')->references('id')->on('people');
            $table->integer('patient_id')->unsigned();
            $table->foreign('patient_id')->references('id')->on('people');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('people');
        });
    }

    public function down()
    {
        Schema::table('agendas', function (Blueprint $table) {
            $table->dropForeign(['doctor_id']);
            $table->dropForeign(['patient_id']);
            $table->dropForeign(['created_by']);
        });
    }
}
