<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExamsTableAddForeignKey extends Migration
{
    public function up()
    {
        Schema::table('exams', function (Blueprint $table) {
            $table->integer('appointment_id')->unsigned()->nullable();
            $table->foreign('appointment_id')->references('id')->on('appointments');
            $table->integer('type_id')->unsigned();
            $table->foreign('type_id')->references('id')->on('types');
        });
    }

    public function down()
    {
        Schema::table('exams', function (Blueprint $table) {
            $table->dropForeign(['appointment_id']);
            $table->dropForeign(['type_id']);
        });
    }
}
