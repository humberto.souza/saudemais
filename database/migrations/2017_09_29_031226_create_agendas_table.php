<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{

    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->time('start');
            $table->time('end');
            $table->integer('agendable_id')->nullable();
            $table->string('agendable_type');
            $table->string('exam')->nullable();
            $table->enum('status',[
                'marcado',
                'aguardando',
                'em atendimento',
                'finalizado',
                'faltou',
                'cancelado'])->default('marcado');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
