<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterExpedientsTableAddForeignKey extends Migration
{
    public function up()
    {
        Schema::table('expedients', function (Blueprint $table) {
            $table->integer('person_id')->unsigned();
            $table->foreign('person_id')->references('id')->on('people');
        });
    }

    public function down()
    {
        Schema::table('expedients', function (Blueprint $table) {
            $table->dropForeign(['person_id']);
        });
    }
}
