<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTypesTableAddForeignKey extends Migration
{
    public function up()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->integer('role_id')->unsigned();
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    public function down()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
        });
    }
}
