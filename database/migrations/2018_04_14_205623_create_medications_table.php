<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicationsTable extends Migration
{

    public function up()
    {
        Schema::create('medications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('factoryName');
            $table->string('manufacturer');
            $table->string('type');
            $table->timestamps();
        });

        Schema::create('appointment_medication', function (Blueprint $table) {
            $table->integer('appointment_id');
            $table->integer('medication_id');
            $table->primary(['appointment_id','medication_id']);
            $table->text('prescription');
        });
    }

    public function down()
    {
        Schema::dropIfExists('medications');
        Schema::dropIfExists('appointment_medication');
    }
}
