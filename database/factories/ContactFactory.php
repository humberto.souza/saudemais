<?php

use Faker\Generator as Faker;

$factory->define(Clinic\Contact::class, function (Faker $faker) {
    
    static $password;

    return [
        'landline' => $faker->numberBetween($min = 32312332, $max = 32354532),
        'phone' => $faker->numberBetween($min = 984555442, $max = 999999999),
    ];
});
