<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Clinic\Person;
use Clinic\Role;

$factory->define(Clinic\Agenda::class, function (Faker $faker) {

    $patients = $faker->randomElement(Person::people('patients')->pluck('id')->all());
    $doctors = $faker->randomElement(Person::people('doctors')->where('category_id',3)->pluck('person_id')->all());
	$recepcionist = $faker->randomElement(Person::people('recepcionist')->pluck('person_id')->all());
    
    $interval = $faker->randomElements(['00','15', '30','45']);

    $start = $faker
        ->dateTimeBetween($startDate = '07:00', $endDate = '17:59', $timezone = 'America/Sao_Paulo')->format('H:').$interval[0];

    $date = $faker
        ->dateTimeBetween($startDate = 'now', $endDate = '+1 week', $timezone = 'America/Sao_Paulo')->format('Y-m-d');

    $dt = Carbon::parse($date);
    

    while($dt->isWeekend())
    {
        $date = $faker
                    ->dateTimeBetween($startDate = 'now', $endDate = '+1 week', $timezone = 'America/Sao_Paulo')->format('Y-m-d');

        $dt = Carbon::parse($date);

        if($dt->isWeekend()){
            continue;
        };

        break;
    };

    return [
        'date' => $dt->format('Y-m-d'),
        'start' => $start,
        'end' => date('H:i', strtotime('+15 minute', strtotime($start))),
        //'agendable_id' => Clinic\Appointment::create()->id,
        'agendable_type' => 'Clinic\\Appointment',
        'doctor_id' => $doctors,
        'patient_id' => $patients,
        'created_by' => $recepcionist,
    ];
});