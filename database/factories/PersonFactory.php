<?php

use Faker\Generator as Faker;

/* @var Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Clinic\Person::class, function (Faker $faker) {
    
    $gender = $faker->randomElements(['male', 'female']);

    return [
        'firstName' => $faker->firstName($gender),
        'lastName' => $faker->lastName,
        'birthday' => $faker->date($format = 'Y-m-d', $max = '1980-12-30'),
        'gender' => $gender === ['male'] ? 'male' : 'female' ,
		'cpf' => $faker->numberBetween($min = 11111111111, $max = 99999999999),
		'rg' => $faker->numberBetween($min = 11111111, $max = 99999999),
		'streetName' => $faker->streetName,
		'number' => $faker->numberBetween($min = 1, $max = 999),
		'address' => $faker->streetName,
		'secondaryAddress' => $faker->secondaryAddress,
		'postcode' => $faker->numberBetween($min = 69051110, $max = 69760000),
    ];
});
