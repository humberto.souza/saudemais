<?php

use Faker\Generator as Faker;

$factory->define(Clinic\Expedient::class, function (Faker $faker) {

	$expedient = $faker->randomElements(['matutino','vespertino','noturno', 'integral']);
	
	if($expedient[0] == 'matutino') {
		$start = '07:00';
		$end = '11:00';
	} else if($expedient[0] == 'vespertino') {
		$start = '13:00';
		$end = '17:00';
	} else if($expedient[0] == 'noturno') {
		$start = '18:00';
		$end = '22:00';
	} else if ($expedient[0] == 'integral') {
		$start = '08:00';
		$end = '18:00';
	}

    return [
        'start' => $start,
        'end' => $end,
    ];
});
