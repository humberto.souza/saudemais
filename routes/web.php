<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/home', function () {
    return redirect('/agenda');
});

Auth::routes();

Route::get('/', 'AgendaController@index')->name('agenda');

//Agenda
Route::patch('/changeAgendaStatus/{agenda}', 'AgendaController@changeStatus');

Route::get('/agenda', 'AgendaController@index')->name('agenda');

Route::get('/agenda/create', 'AgendaController@create');

Route::post('/agenda', 'AgendaController@store');

Route::get('/agenda/{agenda}', 'AgendaController@show');

Route::get('/search/agenda','AgendaController@search');

Route::get('/agenda/{agenda}/edit', 'AgendaController@edit');

Route::patch('/agenda/{agenda}','AgendaController@update');

Route::delete('/agenda/{agenda}','AgendaController@destroy');

Route::get('/agenda/request/{appointment}/{request}', 'AgendaController@request');

//JSON Feed
Route::get('/agenda/weekday/{doctor}', 'AgendaController@weekday');

Route::get('/agenda/doctors/{role}', 'AgendaController@doctorsByRole');

Route::get('/schedules', 'AgendaController@schedules');

Route::get('/agenda/question/{patientType}', 'AgendaController@patientType');

Route::get('/schedules/time-limit', 'AgendaController@getTimeLimit');

Route::get('/schedules/{doctor}', 'AgendaController@doctorSchedules');

Route::get('/technicians/{type}', 'AgendaController@technicians');

Route::get('/validator/time', 'AgendaController@timeValidator');

Route::get('/validator/date', 'AgendaController@dateValidator');

Route::get('/validator/patient/date', 'AgendaController@validatorForDoctorAsPatient');

//Appointment

Route::get('/patient/appointments/{patient}', 'AppointmentController@index');

Route::get('/appointments/create', 'AppointmentController@create');

Route::post('/appointments', 'AppointmentController@store');

Route::get('/appointments/{appointment}', 'AppointmentController@show');

Route::get('/appointments/{appointment}/edit','AppointmentController@edit');

Route::patch('/appointments/{appointment}','AppointmentController@update');

Route::get('/medications/{type}', 'AppointmentController@medications');

Route::get('/history/{agenda}', 'AppointmentController@history');

Route::get('/print/appointment/{type}', 'AppointmentController@print');

//Exam

Route::get('/exam/{person}','ExamController@index');

Route::get('/exams/create', 'ExamController@create');

Route::post('/exams', 'ExamController@store');

Route::get('/exams/{exam}', 'ExamController@show');

Route::get('/exams/{exam}/edit','ExamController@edit');

Route::patch('/exams/{exam}','ExamController@update');

Route::get('/print/exam/{exam}', 'ExamController@print');

//Patient

Route::get('/patients','PatientController@index')->name('patients');

Route::get('/patients/create','PatientController@create');

Route::post('/patients','PatientController@store');

Route::get('/patients/{person}','PatientController@show');

Route::get('/search/patient','PatientController@search');

Route::get('/patients/{person}/edit','PatientController@edit');

Route::patch('/patients/{person}','PatientController@update');

Route::delete('/patients/{person}','PatientController@destroy');

//Employee

Route::get('/employees','EmployeeController@index')->name('employees');

Route::get('/employees/create','EmployeeController@create');

Route::post('/employees','EmployeeController@store');

Route::get('/employees/{person}','EmployeeController@show');

Route::get('/search/employee','EmployeeController@search');

Route::get('/employees/{person}/edit','EmployeeController@edit')->name('employeesEdit');

Route::patch('/employees/{person}','EmployeeController@update');

Route::delete('/employees/{person}','EmployeeController@destroy');

Route::get('/employees/{person}/expedients','EmployeeController@expedients');

Route::post('/employee/resetpassword','EmployeeController@resetPassword');

Route::get('/employee/resetpassword', function(){
	return view('auth.passwords.reset');
});