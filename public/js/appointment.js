$('#medication').on('change', function(){ 
	getMedications(); 
	$('#medication-table').attr('hidden','');
	$('#medication-manufacturer').text("");
	$('#medication-factoryName').text("");
});

function getMedications()
{
	var medication;

	var type = $('#medication').val();
	$('.medication-item').remove();
	$.get('/medications/'+type, function(medications){

		$('#appointment-create-box').append("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");

		for (var i in medications)
		{
			if($('#btn-remove-'+medications[i].name).length >= 1)
			{
				$('#medication-list')
				.append("<li id='"+ i +"'class='medication-item'>"+
					"<button id='btn-"+medications[i].name+"' class='btn btn-default medication-btn' value='"+medications[i].id+"' disabled>"+ medications[i].name +"</button></li>");
				continue;
			}


			$('#medication-list')
				.append("<li id='"+ i +"'class='medication-item'>"+
					"<button id='btn-"+medications[i].name+"' class='btn btn-default medication-btn' value='"+medications[i].id+"'>"+ medications[i].name +"</button></li>");
		};
		addMedication();

		$('.medication-item').click(function(){
			var id = $(this).attr('id');
			$('#'+medications[i].name+'-input').val(medications[id].id);
			$('#medication-table').removeAttr('hidden');
			$('#medication-manufacturer').text( medications[id].manufacturer );
			$('#medication-factoryName').text( medications[id].factoryName );
		});
		

	}).always(function(){
		$('.overlay').remove();
	});
}

$('#exam-check').change(function(){
	if ($(this).is(':checked'))
	{
		$('#exam-list').removeAttr('hidden','');
		$('.exam-item').removeAttr('disabled');
	} else {
		$('#exam-list').attr('hidden','');
		$('.exam-item').attr('disabled','');
	};
});

function addMedication(){
	$('.medication-btn').click(function(){

		$(this).attr('disabled','');

		if($('#item-'+$(this).text()).length >= 1)
			return false;


		$('#prescription-fieldset').after(
			"<div id='item-"+$(this).text()+"' class='row'>"+
			"<div class='col-md-12'>"+
			"<div class='form-group'>"+
			"<label for='prescription'>"+$(this).text()+" <span id='btn-remove-"+$(this).text()+"' class='fa fa-remove'>Remover</span></label>"+
            "<textarea rows='2' id='"+$(this).text()+"-prescription' name='prescription[]' class='form-control' required></textarea>"+
            "<input id='"+$(this).text()+"-input' type='hidden' name='medication_id[]' value="+$(this).val()+"></input>"+
            "</div>"+
            "</div>"+
    	    "</div>"
        );
        var medication = $(this).text();

        $('#btn-remove-'+medication).click(function(){
        	$('#item-'+medication).remove();
        	$('#btn-'+medication).removeAttr('disabled');
        });
	});
	
}
function removeButton()
{
	$('.btn-remove').click(function(){
		var button = $(this).attr('id');

		if($(this).css('color') == 'rgb(0, 166, 90)')
		{
			$(this).text('Remover').css('color','#dd4b39').attr('class', 'fa fa-remove btn-remove');
			$('#'+button+'-prescription').removeAttr('disabled');
			$('#'+button+'-input').attr('name','existing_medication_id[]');
		} else {
			$(this).text('Incluir').css('color','#00a65a').attr('class', 'fa fa-check btn-remove');;
			$('#'+button+'-prescription').attr('disabled','');
			$('#'+button+'-input').attr('name','canceled_medication_id[]');
		};
	});
}

$('#btn-print').click(function(){ 
	window.print();
});
