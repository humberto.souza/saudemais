$(document).ready(function() {

	var employee_id = $('#employee_id').text();
	var category_id = $('#category_id').text();
	
	$("#btn-filter").click(function(event){
		event.preventDefault();
		filter();
	});

	$('#question-filter').click(function(){
		filterCheck();
	});

	$('#patient-filter').on('change',function(){

		var except;

		if($('#question-filter').is(':checked'))
		{
			except = $('#patient-filter').val();
			getDoctorsFilter(except);
		}

	});

	$("#index-reset").click(function(){
		$('#index-calendar').fullCalendar('destroy');
		showSchedule('/schedules');
	});

	if ($('#category_id').text() != 3 && $('#category_id').text() != 5 )
	{
		showSchedule('/schedules');
	} else {
		showSchedule('/schedules/'+employee_id);
	}
});

function showSchedule(url){

	$('#index-calendar').fullCalendar({
		header: { center: 'month agenda list', left: 'title', right: 'today prev,next' },
		allDayDefault: false,
		events : url,
		windowResize: function(view) {},
	    allDaySlot: false,
	    eventLimit: 4,
	    hiddenDays: [0,7],
	    minTime: '07:00:00',
	    maxTime: '22:00:00',
	    dayClick: function(date, jsEvent, view, resourceObj) {
		    $('#index-calendar').fullCalendar('changeView', 'list', {});
		    $('#index-calendar').fullCalendar( 'gotoDate',date.format());
		}
	});
}

function filter()
{
	var agendable = $("#agendable-filter").val();
	var status = $("#status-filter").val();
	var patient_id = $("#patient-filter").val();
	var doctor_id = $("#doctor-filter").val();

	$.get('/schedules', { agendable : agendable, status : status, patient_id : patient_id, doctor_id : doctor_id } , function(schedules){
		$('#index-calendar').fullCalendar('destroy');
		showSchedule(schedules);
	});
}

function filterCheck()
{
	var patientType;

	if($('#question-filter').is(':checked'))
		patientType = 'doctors';
	else
		patientType = 'patients';

	getPatientsFilter(patientType);
}

function getPatientsFilter(patientType)
{
	$.get('/agenda/question/'+ patientType, function(patients){
		
		$('#box-filter').append("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
		
		if(patients.length == 0)
		{
			$('#patient-filter').text("");
			$('#patient-filter').append("<option disabled selected>Nenhum registro encontrado.</option");
			return false;
		}
		
		$('#patient-filter').text("");
		$('#patient-filter').append('<option disabled selected>Selecione</option>');
		$('#doctor-filter').text("");
		$('#doctor-filter').append('<option disabled selected>Selecione</option>');

		//Except '0', that will retreave all doctors except the one who has the ID 0 (ID 0 doesn't exist)
		getDoctorsFilter(0);

		for(var i in patients)
		{
			if($('#question-filter').is(':checked'))
				$('#patient-filter').append("<option value='"+patients[i].person_id+"'>"+patients[i].firstName+" "+ patients[i].lastName+"</option>");
			else
				$('#patient-filter').append("<option value='"+patients[i].id+"'>"+patients[i].firstName+" "+ patients[i].lastName+"</option>");
		}
	}).always(function(){
		$('.overlay').remove();
	});
}

function getDoctorsFilter(except)
{
	/* '/agenda/doctors' needs to pass a Role id, so put '1'. When hits the controller, it'll check first if 
	the doctorsFilter is set. And so, will retrieve all the doctors, except the id passed*/
	var all = 1;

	$.get('/agenda/doctors/'+all, { except: except, doctorsFilter : 'all' }, function(employees){
		
		$('#box-filter').append("<div class='overlay'><i class='fa fa-refresh fa-spin'></i></div>");
		
		if(employees.length == 0)
		{
			$('#doctor-filter').text("");
			$('#doctor-filter').append("<option disabled selected>Nenhum registro encontrado.</option");
			return false;
		}

		$('#doctor-filter').text("");
		$('#doctor-filter').append('<option disabled selected>Selecione</option>');

		for(var i in employees)
		{
			$('#doctor-filter').append("<option value='"+employees[i].person_id+"'>"+employees[i].firstName+" "+ employees[i].lastName+"</option>");
		}

	}).always(function(){
		$('.overlay').remove();
	});

}