//Form - Button + (plus) and - (minus)
$('.datatable-btn').click(function(){
	if( $(this).attr('id') == 'datatable-btn-close')
	{
		$(this).attr({
			'id':'datatable-btn-open',
			'class':'datatable-btn glyphicon glyphicon-minus'
		})
		.css('backgroundColor','#ce373f')
		.parents('tr').next().removeAttr('hidden');
	} else {
		$(this).attr({
			'id':'datatable-btn-close',
			'class':'datatable-btn glyphicon glyphicon-plus'
		}).css('backgroundColor','#007dcc')
		.parents('tr').next().attr('hidden','');	
	}	
});

//Form - gender icon
$('#male-gender').click(function(){
	$('#gender').attr('class','fa fa-mars');
});

$('#female-gender').click(function(){
	$('#gender').attr('class','fa fa-venus');
});

//Form - Office Hours
$('.controlWeekday').click(function(){
	if ($(this).is(':checked'))
		unlock( $(this).attr('id'));
	else 
		lock($(this).attr('id'));
});

function unlock(obj){
    $('.'+obj).removeAttr('disabled');
};

function lock(obj){
    $('.'+obj).attr('disabled','');
};

//Form - hide office hours if is not a doctor

$('#jobTitle').on('change',function(){
	
	if( $(this).val() >= 3 ) //Value 3 means the Category is "Saúde"
	{
		$('#officeHours').removeAttr('hidden');
	}
	else
		$('#officeHours').attr('hidden','');

});

function disablePerson(){
	$('#confirm-remove-btn').click(function(){
		$("#confirm-remove").attr('hidden','');
		$("#form-remove").removeAttr('hidden');

		$("#confirm-remove-btn-no").click(function(){
			$("#form-remove").attr('hidden','');
			$("#confirm-remove").removeAttr('hidden');
		});
	});
}

