<!DOCTYPE html>

<html lang="pt-br">
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Saúde Mais</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet"
  href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href=" {{asset('/bower_components/font-awesome/css/font-awesome.min.css')}} ">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/dist/css/AdminLTE.min.css') }}">

  <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/dist/css/skins/skin-blue.min.css') }}">
  
  <link rel='stylesheet' href="{{ asset('/bower_components/fullcalendar/dist/fullcalendar.css') }}" />
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link href="{{ asset('css/mycss.css') }}" rel="stylesheet">
  <link href="{{ asset('css/datatable-btn.css') }}" rel="stylesheet">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  
  <div class="wrapper">

    @include('layouts.header')

    @guest
    @else
      @include('layouts.sidebar')
    @endguest
    
  <div class="content-wrapper">

    <section class="content container-fluid">

      @if($flash = session('message'))
        @php $alert = session('alert') @endphp
        <div class="alert alert-{{$alert}} alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{$flash}}
        </div>
      @endif

      @if($errors->all())
        <div class="alert alert-danger">
          <ul class="list-unstyled">
            @foreach($errors->all() as $error)
              <li>{{$error}}</li>
            @endforeach
          </ul>
        </div>
      @endif
      
      @yield('content')

    </section>

  </div>

    @include('layouts.footer')

  <!-- jQuery 3 -->
  <script src="/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/bower_components/admin-lte/dist/js/adminlte.min.js"></script>

  <!-- FullCallendar-->
  <script src="{{ asset('js/moment.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/fullcalendar.js') }}" type="text/javascript"></script>
  <script src="{{ asset('/bower_components/fullcalendar/dist/locale/pt-br.js') }}"></script>

  <!-- My own Javascript -->
  <script src="{{ asset('js/script.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/calendar.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/index-calendar.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/appointment.js') }}" type="text/javascript"></script>

</body>
</html>