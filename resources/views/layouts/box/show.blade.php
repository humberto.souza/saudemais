<div class="box box-primary">
    
    <div class="box-header with-border">
        <h3 class="box-title">{{ $title }}</h3>
        
        <div class="box-tools pull-right">
            <a id="button-panel-heading" href="{{ $url }}"><span class="fa fa-pencil"> Editar</span></a>
        </div>

    </div>
    <div class="box-body">

        {{ $boxBody }}
        
    </div>

</div>

    @if( \Request::is('agenda/*') )

        {{$changeStatus}}

    @endif

    @include('layouts.box.back')
