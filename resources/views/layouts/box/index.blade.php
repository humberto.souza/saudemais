<div class="box box-primary">

    <div class="box-header with-border">
        <h3 class="box-title">{{ $title }}</h3>
        
        <div class="box-tools pull-right">
            <a id="button-panel-heading" href="{{ $url }}" class="btn btn-xs btn-primary"><span class="fa fa-user-plus"></span> Adicionar</a>
        </div>

    </div>
    <div class="box-body">

        <form id="search-group" class="form-inline" action="{{ $searchUrl }}">

            <div class="form-group">
                <label for="column">Buscar por</label>
                <select id="column" class="form-control" name="column">
                    <option value="cpf">CPF</option>
                    @if(str_contains(request()->path(),'employee'))
                        <option value="jobTitle">Cargo</option>
                    @endif
                    <option value="firstName">Nome</option>
                    <option value="lastName">Sobrenome</option>
                </select>
            </div>

            <div class="form-group">
                <input type="text" class="form-control" id="search-box" name="name" required>
            </div>

            <button type="submit" class="btn btn-default">buscar</button>

        </form>

        @if(session('result') != null && request()->path() != 'patients' && request()->path() != 'employees')

        <div>
            <p>Foram encontrados {{session('result')}} registros.</p>
        </div>

        @endif

        <table class="table table-striped table-bordered">
            <thead>
                <tr><th colspan="3">Nome</th></tr>
            </thead>
            <tbody>
            	{{ $tbody }}
            </tbody>   
        </table>

    </div>
</div>

