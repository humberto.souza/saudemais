@php
	$string = URL::current();
	$urlArray = explode('/',$string);
	
	if( collect($urlArray)->contains('search') ) {
		
		if(collect($urlArray)->contains('patient'))
			$url = '/patients';
		else
			$url = '/employees';		

	} else if( collect($urlArray)->contains('appointments') && !collect($urlArray)->contains('edit')) {
		$appointment_id = collect($urlArray)->last();
		$agenda_id = \Clinic\Appointment::find($appointment_id)->agenda->first()->id;
		$path = 'agenda';
		array_pop($urlArray);
		array_pop($urlArray);
		array_push($urlArray,$path);
		array_push($urlArray,$agenda_id);
		$url = implode("/",$urlArray);
	} else if(collect($urlArray)->contains('exams') && !collect($urlArray)->contains('edit')){
		$exam_id = collect($urlArray)->last();
		$agenda_id = \Clinic\Exam::find($exam_id)->agenda->first()->id;
		$path = 'agenda';
		array_pop($urlArray);
		array_pop($urlArray);
		array_push($urlArray,$path);
		array_push($urlArray,$agenda_id);
		$url = implode("/",$urlArray);
	} else {
		array_pop($urlArray);
		$url = implode("/",$urlArray);
	}

@endphp
<div id="url-back">
    <p><a href="{{$url}}" class="btn btn-default">Voltar</a></p>
</div>