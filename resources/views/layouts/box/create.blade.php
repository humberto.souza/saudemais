<div class="box box-{{ $skin }}">
 
    <div class="box-body">

	    <form action="{{ $url }}" method="POST">

				{{ csrf_field() }}

				<fieldset>
					<legend>Informações Básicas</legend>
			        <div class="row">
			        	<div class="form-group col-sm-6">
			               <label for="firstName">Nome</label>
			               <input id="firstName" type="text" name="firstName" class="form-control" required>
			            </div>
			            
			            <div class="form-group col-sm-6">
			                <label for="lastName">Sobrenome</label>
			                <input id="lastName" type="text" name="lastName" class="form-control" required>
			            </div>
			        </div>

			        <div class="row">

				        <div class="form-group col-sm-6 col-md-3 col-lg-3">
				            <label for="birthday">Data de Nascimento</label>
							<div class="input-group">
	      						<div class="input-group-addon">
	      							<i class="fa fa-calendar"></i>
	      						</div>
	      						<input id="birthday" type="date" name="birthday" class="form-control" required>
	      					</div>
				        </div>
			        
			        	<div class="form-group col-sm-6 col-md-3 col-lg-3">
			                <label for="cpf">CPF</label>
			                <input id="cpf" type="text" name="cpf" class="form-control" maxlength="14" required>
			            </div>
			            
			            <div class="form-group col-sm-6 col-md-3 col-lg-3">
			                <label for="rg">RG</label>
			                <input id="rg" type="text" name="rg" class="form-control" maxlength="13" required>
			            </div>
			        </div>
			           
			        <div class="form-group">
			            <label for="gender">Sexo <span id="gender"></span></label>

			            <div>
			                <label class="radio-inline">
			                    <input id="male-gender" type="radio" name="gender" value="male"> Masculino
			                </label>
			                <label class="radio-inline">
			                    <input id="female-gender" type="radio" name="gender" value="female"> Feminino
			                </label>
			            </div>
			        </div>
			    </fieldset>

			    <fieldset>
			    	<legend>Endereço</legend>
			        <div class="row">
			            <div class="form-group col-sm-3">
			                <label for="postcode">CEP</label>
			                <input id="postcode" type="text" name="postcode" class="form-control" maxlength="9" required>
			            </div>

			        	<div class="form-group col-sm-4">
			                <label for="streetName">Rua</label>
			                <input id="streetName" type="text" name="streetName" class="form-control" required>
			            </div>
			            
			            <div class="form-group col-sm-3">
			                <label for="number">Nº</label>
			                <input id="number" type="text" name="number" class="form-control" required>
			            </div>
					</div>
					
					<div class="row">
			            <div class="form-group col-sm-6">
			                <label for="address">Bairro</label>
			                <input id="address" type="text" name="address" class="form-control" required>
			            </div>

			            <div class="form-group col-sm-6">
			                <label for="secondaryAddress">Complemento</label>
			                <input id="secondaryAddress" type="text" name="secondaryAddress" class="form-control">
			            </div>

			        </div>
			    </fieldset>

			    <fieldset>
			    	<legend>Contato</legend>
			        <div class="row">
				        <div class="form-group col-sm-4">
					        <label for="email">E-mail</label>
				     		<div class="input-group">
	      						<div class="input-group-addon">
	      							<i class="fa fa-envelope"></i>
	      						</div>
					            <input id="email" type="email" name="email" class="form-control">
				        	</div>
				        </div>

				        <div class="form-group col-sm-4">
				            <label for="phone">Celular</label>
				            <div class="input-group">
	      						<div class="input-group-addon">
	      							<i class="fa fa-mobile-phone"></i>
	      						</div>
	      						<input id="phone" type="tel" name="phone" class="form-control">
	      					</div>
				        </div>

				        <div class="form-group col-sm-4">
				            <label for="landline">Telefone</label>
				            <div class="input-group">
	      						<div class="input-group-addon">
	      							<i class="fa fa-phone"></i>
	      						</div>
				            	<input id="landline" type="tel" name="landline" class="form-control">
				            </div>
				        </div>

			    	</div>

			    </fieldset>

			    @if(request()->path() == 'employees/create')

				    <fieldset>
				    	<legend>Dados Profissionais</legend>
				    	
				    	<div class="row">
				    		<div class="form-group col-sm-4">
					        	<label for="jobTitle">Cargo</label>
					            
					            <select id="jobTitle" name="jobTitle" class="form-control">
					            	<option selected value="0" disabled>Selecione</option>
					            	
					            	{{$foreachRoles}}

					            </select>		      					
					        </div>

						</div>

				    </fieldset>
				    
				    <fieldset id="officeHours" hidden>
				    	<legend>Horário de Atendimento</legend>

				    	{{$forWeekdays}}

				    </fieldset>

			    @endif
			    
	        	<button class="btn btn-primary">Cadastrar</button>

	    	</form>

    </div>
</div>