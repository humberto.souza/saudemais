<div class="box box-{{ $skin }}">
    
    <div class="box-header with-border">
        <h3 class="box-title">Ficha de {{$firstName}} {{$lastName}}</h3>
    </div>

    <div class="box-body">

	    <form action="{{ $url }}" method="POST">

			{{ csrf_field() }}

			{{ method_field('PATCH') }}

			<div class="row">
	        	<div class="form-group col-sm-6">
	               <label for="firstName">Nome</label>
	               <input id="firstName" type="text" name="firstName" value="{{$firstName}}" class="form-control" required>
	            </div>
	            
	            <div class="form-group col-sm-6">
	                <label for="lastName">Sobrenome</label>
	                <input id="lastName" type="text" name="lastName" value="{{$lastName}}" class="form-control" required>
	            </div>
	        </div>

		    <div class="row">

		        <div class="form-group col-sm-6 col-md-3 col-lg-3">
		            <label for="birthday">Data de Nascimento</label>
					<div class="input-group">
  						<div class="input-group-addon">
  							<i class="fa fa-calendar"></i>
  						</div>
  						<input id="birthday" type="date" name="birthday" value="{{$birthday}}" class="form-control" required>
  					</div>
		        </div>
	        
	        	<div class="form-group col-sm-6 col-md-3 col-lg-3">
	                <label for="cpf">CPF</label>
	                <input id="cpf" type="text" name="cpf" value="{{$cpf}}" class="form-control" required>
	            </div>
	            
	            <div class="form-group col-sm-6 col-md-3 col-lg-3">
	                <label for="rg">RG</label>
	                <input id="rg" type="text" name="rg" value="{{$rg}}" class="form-control" required>
	            </div>
	        </div>
	           
	        <div class="form-group">
	            <label for="gender">Sexo <span id="gender"></span></label>

	            <div>
	                <label class="radio-inline">
	                    <input id="male-gender" type="radio" name="gender" value="male" {{$gender == 'male' ? 'checked' : ''}}> Masculino
	                </label>
	                <label class="radio-inline">
	                    <input id="female-gender" type="radio" name="gender" value="female" {{$gender == 'female' ? 'checked' : ''}}> Feminino
	                </label>
	            </div>
	        </div>

	        <fieldset>
	        	<legend>Endereço</legend>

		        <div class="row">
		            <div class="form-group col-sm-3">
		                <label for="postcode">CEP</label>
		                <input id="postcode" type="text" name="postcode" value="{{$postcode}}" class="form-control" required>
		            </div>

		        	<div class="form-group col-sm-4">
		                <label for="streetName">Rua</label>
		                <input id="streetName" type="text" name="streetName" value="{{$streetName}}" class="form-control" required>
		            </div>
		            
		            <div class="form-group col-sm-3">
		                <label for="number">Nº</label>
		                <input id="number" type="text" name="number" value="{{$number}}" class="form-control" required>
		            </div>
				</div>
			</fieldset>
			
			<div class="row">
	            <div class="form-group col-sm-6">
	                <label for="address">Bairro</label>
	                <input id="address" type="text" name="address" value="{{$address}}" class="form-control" required>
	            </div>

	            <div class="form-group col-sm-6">
	                <label for="secondaryAddress">Complemento</label>
	                <input id="secondaryAddress" type="text" name="secondaryAddress" value="{{$secondaryAddress}}" class="form-control">
	            </div>

	        </div>

	        <fieldset>
	        	<legend>Contato</legend>

		        <div class="row">
			        <div class="form-group col-sm-4">
				        <label for="email">E-mail</label>
			     		<div class="input-group">
	  						<div class="input-group-addon">
	  							<i class="fa fa-envelope"></i>
	  						</div>
				            <input id="email" type="email" name="email" value="{{$email}}" class="form-control" required>
			        	</div>
			        </div>

			        <div class="form-group col-sm-4">
			            <label for="phone">Celular</label>
			            <div class="input-group">
	  						<div class="input-group-addon">
	  							<i class="fa fa-mobile-phone"></i>
	  						</div>
	  						<input id="phone" type="text" name="phone" value="{{$phone}}" class="form-control" required>
	  					</div>
			        </div>

			        <div class="form-group col-sm-4">
			            <label for="landline">Telefone</label>
			            <div class="input-group">
	  						<div class="input-group-addon">
	  							<i class="fa fa-phone"></i>
	  						</div>
			            	<input id="landline" type="text" name="landline" value="{{$landline}}" class="form-control">
			            </div>
			        </div>

			    </div>

		    </fieldset>

		    @if( \Request::route()->getName() == 'employeesEdit')

				    <fieldset>
				    	<legend>Dados Profissionais</legend>
				    	
				    	<div class="row">
				    		<div class="form-group col-sm-4">
					        	<label for="jobTitle">Cargo</label>
					            
					            <select id="jobTitle" name="jobTitle" class="form-control">

					            	{{$foreachRoles}}

					            </select>		      					
					        </div>

						</div>

				    </fieldset>

				    {{$forWeekdays}}
				    
			@endif

	        <button class="btn btn-warning">Alterar</button>

	    </form>

    </div>

</div>

@include('layouts.box.back')