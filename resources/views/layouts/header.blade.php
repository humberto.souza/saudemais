

<header class="main-header">

    <!-- Logo -->
    <a href="/agenda" class="logo">     
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Saúde <span class="glyphicon glyphicon-plus"></span></b></span>
    </a>

    <!-- Header Navbar -->

    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->

      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            @guest
            @else
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
            role="button" aria-expanded="false">
                {{ Auth::user()->person->firstName }} {{ Auth::user()->person->reducedName() }}<span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-footer">
                  <a href="/employees/{{Auth::user()->person->id}}">
                      Informações Pessoais
                  </a>
              </li>

              <li class="user-footer">
                  <a href="/employees/{{Auth::user()->person->id}}/edit">
                      Editar Perfil
                  </a>
              </li>

              <li class="user-footer">
                  <a href="/employee/resetpassword">
                      Alterar Senha
                  </a>
              </li>

              <li class="user-footer">
                  <a class="btn btn-default" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Sair
                  </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
              </li>
            </ul>
          </li>
          @endguest
        </ul>
      </div>
    </nav>
</header>
