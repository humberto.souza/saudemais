<aside class="main-sidebar">

    <section class="sidebar">
      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        @if(\Auth::user()->person->roles->first()->id >= 2)
          <li class="treeview">
            <a href="#"><i class="fa fa-calendar"></i> <span>Agenda</span>
              <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
              @if(\Auth::user()->person->roles->first()->id == 2)
                <li><a href="/agenda/create"><i class="fa fa-calendar-plus-o"></i>Agendar</a></li>
              @endif
              <li><a href="/agenda"><i class="fa fa-stethoscope"></i>Consultas e Exames</a></li>
            </ul>
          </li>
        @endif
        <li><a href="{{ route ('patients') }}"><i class="fa fa-user"></i> <span>Pacientes</span></a></li>
        <li><a href="{{ route ('employees') }}"><i class="fa fa-user"></i> <span>Funcionários</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>