@extends('layouts.master')

@section('content')

	@if($flash = session('message'))
    	<div class="alert alert-info alert-dismissible">
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	        </button>
	        {{$flash}}   
	    </div>
	@endif
    <div class="row">
        <div class="col-lg-8">
            <div id="appointment-create-box" class="box box-primary">
               
                <div class="box-header with-border">
                    <h4 class="box-title">{{ $appointment->agenda->first()->patient->firstName ." ". $appointment->agenda->first()->patient->lastName }}</h4>
                    <p>{{$appointment->agenda->first()->patient->birthday}}</p>
                    <ul class="list-unstyled">
                        <li>Dt nascimento: {{$appointment->agenda->first()->patient->date}}</li>
                        <li>Telefone: {{$appointment->agenda->first()->patient->contact->landline}}</li>
                        <li>Celular: {{$appointment->agenda->first()->patient->contact->phone}}</li>
                        <li>E-mail: {{$appointment->agenda->first()->patient->contact->email}}</li>
                    </ul>
                </div>

                <div class="box-body">
                    <form action="/appointments/{{$appointment->id}}" method="POST">

                        {{ csrf_field() }}

                        {{ method_field('PATCH') }}

                        <div id="symptoms-box" class="form-group">
                            <label for="symptoms">Sintomas</label>
                            <textarea rows="4" id="symptoms" name="symptoms" class="form-control" required>{{$appointment->symptoms}}</textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="medication">Medicamentos</label>
                            <select id="medication" name="medication" class="form-control" required>
                                <option disabled selected>Selecione</option>
                                <option value="Analgésico">Analgésicos</option>
                                <option value="Antiinflamatório">Anti-inflamatórios</option>
                                <option value="Antialérgico">Antialérgicos</option>
                                <option value="Antibiótico">Antibióticos</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <ul id="medication-list" class="list-unstyled list-inline">
                            </ul>
                        </div>                    

                        <h2 id="medication-header"></h2>
                        <table id="medication-table" class="table" hidden>
                            <tr>
                                <th>Fabricante</th>
                                <th>Nome Genérico</th>
                            </tr>
                            <tr>
                                <td id="medication-manufacturer"></td>
                                <td id="medication-factoryName"></td>
                            </tr>
                        </table>

                        <fieldset>
                            <legend id="prescription-fieldset">Receituário</legend>

                            @foreach($appointment->medications as $a)
                                <div id="item-{{$a->name}}" class="form-group">
                                    <label for="{{$a->name}}-prescription">{{$a->name}} <span id='{{$a->name}}' class='fa fa-remove btn-remove'>Remover</span></label>
                                    <textarea rows="2" id="{{$a->name}}-prescription" name="existing_prescription[]" type="textarea" class="form-control" required>{{$a->pivot->prescription}}</textarea>
                                    <input id='{{$a->name}}-input' type='hidden' name='existing_medication_id[]' value="{{$a->pivot->medication_id}}">
                                </div>
                            @endforeach
                        </fieldset>
                        

                        <fieldset id="exam-list" class="examRequest-box">

                            <legend>Exames</legend>
                            @foreach($exams as $e)
                                <div class="checkbox">
                                    <label>
                                        <input class="exam-item" type="checkbox" name="exams[]" value="{{$e->type}}" {{$requests->contains($e->type) ? 'checked' : ''}}> 
                                            {{$e->type}}
                                    </label>
                                </div>
                            @endforeach 

                        </fieldset>
                        
                        <button type="submit" class="btn btn-warning">Alterar</button>

                    </form>
                </div>
            </div>

            @include('layouts.box.back')

        </div>
        
    </div>

@endsection