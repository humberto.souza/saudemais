@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-8">
            <div id="appointment-create-box" class="box box-primary">
               
                <div class="printable box-header with-border">
                    <h4 class="box-title">{{ $patient->firstName ." ". $patient->lastName }}</h4>
                    <p>{{$patient->birthday}}</p>
                    <ul class="list-unstyled">
                        <li>Dt nascimento: {{$patient->date}}</li>
                        <li>Telefone: {{$patient->contact->landline}}</li>
                        <li>Celular: {{$patient->contact->phone}}</li>
                        <li>E-mail: {{$patient->contact->email}}</li>
                    </ul>
                </div>

                <div class="box-body">
                    <form action="/appointments" method="POST">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <input type="hidden" value="{{$agenda->id}}" name="agenda_id">
                        </div>  

                        <div id="symptoms-box" class="form-group">
                            <label for="symptoms">Sintomas</label>
                            <textarea rows="4" id="symptoms" name="symptoms" class="form-control" required></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="medication">Medicamentos</label>
                            <select id="medication" name="medication" class="form-control">
                                <option disabled selected>Selecione</option>
                                <option value="Analgésico">Analgésicos</option>
                                <option value="Antiinflamatório">Anti-inflamatórios</option>
                                <option value="Antialérgico">Antialérgicos</option>
                                <option value="Antibiótico">Antibióticos</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <ul id="medication-list" class="list-unstyled list-inline">
                            </ul>
                        </div>                    

                        <h2 id="medication-header"></h2>
                        <table id="medication-table" class="table" hidden>
                            <tr>
                                <th>Fabricante</th>
                                <th>Nome Genérico</th>
                            </tr>
                            <tr>
                                <td id="medication-manufacturer"></td>
                                <td id="medication-factoryName"></td>
                            </tr>
                        </table>

                        <fieldset>
                            <legend id="prescription-fieldset">Receituário</legend>
                        </fieldset>
                        
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input id="exam-check" type="checkbox"> Solicitar Exame?
                                </label>
                            </div>
                        </div>

                        <fieldset id="exam-list" class="examRequest-box" hidden>

                            <legend>Exames</legend>
                            @foreach($exams as $e)
                                <div class="checkbox">
                                    <label>
                                        <input class="exam-item" type="checkbox" name="exams[]" value="{{$e->type}}" disabled> {{$e->type}}
                                    </label>
                                </div>
                            @endforeach    
                        </fieldset>
                                            
                        <button type="submit" class="btn btn-primary">Finalizar</button>
                    
                    </form>
                </div>
            </div>
        </div>
        
    </div>

@endsection