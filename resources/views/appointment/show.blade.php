@extends('layouts.master')

@section('content')
    
    @component('layouts.box.show')
        
        @slot('title')
            {{ $patient->firstName ." ". $patient->lastName }}
        @endslot
            
        @slot('url')
           /appointments/{{$appointment->id}}/edit
        @endslot
        
        @slot('boxBody')
            <ul class="list-unstyled">
                <li>Dt nascimento: {{$patient->date}}</li>
                <li>Telefone: {{$patient->contact->landline}}</li>
                <li>Celular: {{$patient->contact->phone}}</li>
                <li>E-mail: {{$patient->contact->email}}</li>
            </ul>
            @if(\Auth::user()->person->roles->first()->id != 2)
                <div class="printable">
                	<h4><b>Sintomas</b></h4>
                    <p>{{$appointment->symptoms}}</p>
                	<h4><b>Prescrição</b></h4>
                    @if($appointment->medications->isEmpty())
                        <p>Sem registros.</p>
                    @else
                        <ul>
                            @foreach($appointment->medications as $prescription)
                                <li><p><strong>{{$prescription->name}}</strong>: {{$prescription->pivot->prescription}}</p></li>
                            @endforeach
                        </ul>
                    @endif
                </div>
            @endif
            
            <div>
                <h4><b>Exames Solicitados</b></h4>
                @if(sizeOf($appointment->requests) > 1)
                    <ul>
                        @foreach($appointment->requests as $request)
                            
                            @if($request == "") 
                                @continue;
                            @endif

                            @if(\Auth::user()->person->roles->first()->id <= 2)
                                <li><a href="/agenda/request/{{$appointment->id}}/{{$loop->iteration}}" target="_blank">{{$request}}</a></li>
                            @else
                                <li>{{$request}}</li>
                            @endif

                        @endforeach
                    </ul>
                @else
                	<p>Nenhum exame solicitado.</p>
                @endif
            </div>
        @endslot

   @endcomponent

@endsection