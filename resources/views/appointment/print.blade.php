@extends('layouts.master')

@section('content')

    <div class="row printable">
        <div class="col-lg-8">

            <div id="print-box" class="box box-solid box-primary">
                
                <div id="print-header" class="box-header with-border">
                    <h4 class="box-title">Saúde Mais <span class="fa fa-stethoscope"></span></h4>
                    <div id="btn-print" class="pull-right">
                        <span class="fa fa-print"> Imprimir</span>
                    </div>
                </div>

                <h4>Paciente - {{ $patient->firstName ." ". $patient->lastName }}</h4>
                @if($type == 'prescription')
                    <div class="box-body">
                        <div class="form-group">
                            <fieldset>
                                <legend>Receituário</legend>
                                @if($prescriptions->isEmpty())
                                    <p>Sem registros.</p>
                                @else
                                    @foreach($prescriptions as $prescription)
                                        <ul class="list-unstyled">
                                            <li><p><strong>{{$prescription->name}} ({{$prescription->factoryName}} - {{$prescription->manufacturer}})</strong>: {{$prescription->pivot->prescription}}</p></li>
                                        </ul>
                                    @endforeach
                                @endif
                            </fieldset>
                        </div>
                    </div>
                @endif

                @if($type == 'exams')
                    <div class="box-body">
                        <div class="form-group">
                            <fieldset>
                                <legend>Exames Solicitados</legend>
                                    @if(collect($requests)->isNotEmpty())
                                    <ul class="list-unstyled">
                                        @foreach($requests as $r)
                                            <li>{{$r}}</li>
                                        @endforeach
                                    </ul>
                                @else
                                    <p>Nenhum exame solicitado.</p>
                                @endif
                            </fieldset>
                        </div>
                    </div>
                @endif

                <hr id="signature">
                <p>{{\Carbon\Carbon::today()->format('d/m/Y')}}</p>
                <p><span class="fa fa-user-md"></span> Dr. {{ $doctor->firstName ." ". $doctor->lastName }} | {{$doctor->roles->first()->jobTitle}}</p>

                <ul class="list-unstyled">
                    <li><span class="fa fa-envelope-o"></span> {{$doctor->contact->email}}</li>
                    <li><span class="fa fa-whatsapp"></span> {{$doctor->contact->phone}}</li>
                </ul>

            </div>

        </div>
    </div>

@endsection