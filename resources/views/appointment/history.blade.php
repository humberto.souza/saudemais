@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-lg-8">
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="info-box">
                    <!-- Apply any bg-* class to to the icon to color it -->
                    <span class="info-box-icon bg-red"><i class="fa fa-heartbeat"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Histórico Clínico</span>
                        <span class="info-box-number">{{$history->first()->patient->firstName}} {{$history->first()->patient->lastName}}</span>
                    </div>
                </div>
            </div>
        </div>
        @if($history->isNotEmpty())
            @foreach($history as $h)
                <div class="box box-primary">

                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>Consulta</strong>
                        <a href="/appointments/{{$h->agendable->id}}" target="_blank"></h3> <span class="fa fa-search"> visualizar</span></a>
                        
                        <div class="box-tools pull-right">
                            <a target="_blank" href="/appointments/{{$h->agendable->id}}/edit"><span class="fa fa-pencil"> Editar</span></a>
                        </div>
                         

                    </div>

                    <div class="box-body">
                        
                        <h4><b>Sintomas</b></h4>
                        {{$h->agendable->symptoms == null ? 'Sem registros.': $h->agendable->symptoms}}
                        <h4><b>Prescrição</b></h4>
                        @if($h->agendable->medications->isEmpty())
                            <p>Sem registros.</p>
                        @else
                            @foreach($h->agendable->medications as $prescription)
                            <ul>
                                <li><p><strong>{{$prescription->name}}</strong>: {{$prescription->pivot->prescription}}</p></li>
                            </ul>
                            @endforeach
                        @endif
                        <h4><b>Exames Solicitados</b></h4>
                        @if($h->agendable->requests == null)
                            Sem registros.
                        @else
                            <ul>
                                @foreach($h->requests as $request)
                                    
                                    @if($request == "") 
                                        @continue;
                                    @endif

                                    <li>{{$request}}</li>

                                @endforeach
                            </ul>
                        @endif
                            @if($h->agendable->exams->isEmpty())
                                <h4><b>Resultados</b></h4>
                                <p>Sem registros.</p>
                            @else
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>ID</th>
                                            <th>Exame</th>
                                            <th>Observações</th>
                                            <th>Resultado</th>
                                        </tr>
                                        @foreach($h->agendable->exams as $e)
                                        <tr>
                                            <td>{{$e->id}}.</td>
                                            <td>{{$e->type->type}}</td>
                                            <td>{{$e->observation}}</td>
                                            <td>{{$e->result}}</td>
                                            <td><a href="/print/exam/{{$e->id}}" target="_blank"><span class="fa fa-print"> Imprimir</span></a></td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                                
                                
                            @endif
                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            Atendido em {{$h->getFormatedDate()}} 
                        </div>    
                    </div>
                    

                </div>   
            @endforeach
        @endif
    </div>
</div>

@endsection