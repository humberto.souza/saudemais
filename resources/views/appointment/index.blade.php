@extends('layouts.master')

@section('content')

	<div class="row">
		<div class="col-lg-6 col-sm-12">
			<div class="info-box">
		  		<!-- Apply any bg-* class to to the icon to color it -->
				<span class="info-box-icon bg-green"><i class="fa fa-calendar"></i></span>
				
				<div class="info-box-content">
		    		<span class="info-box-text">Histórico de Agendamentos</span>
		    		<span class="info-box-number">{{$person->firstName}} {{$person->lastName}}</span>
		    		@isset($appointments->finalizado)
		    			<span>{{$appointments->finalizado}} de {{$appointments->marcado}} finalizados.</span>
	    			@endisset
		  		</div>
			</div>
		</div>
	    <div class="col-lg-4 col-sm-12">
			<form action="/search/agenda">
		        <div id="box-filter" class="box box-primary">
				    <div class="box-body">
				        <label for="column">Buscar por</label>
				        <input type="hidden" name="patient_id" value="{{$person->id}}">
				        <div class="row">
					        <div class="form-group col-sm-6 col-lg-10 ">
					        	<label for="date">Data</label>
			                    <input id="date" type="date" name="date" class="form-control">
				            </div>
				        </div>
			            
			            <div class="row">
				            <div class="form-group col-sm-6 col-lg-10 ">
					        	<label for="doctor_id">Profissional</label>
				                <select id="doctor_id" class="form-control" name="doctor_id">
				                    <option value="" disabled selected>Selecione</option>
				                    @foreach($doctors as $d)
				                    	<option value="{{$d->id}}">{{$d->firstName}} {{$d->lastName}} </option>
				                    @endforeach
				                </select>
				            </div>
			            </div>
						<div class="row">
				            <div class="form-group col-sm-6 col-lg-10 ">
								<button type="submit" class="btn btn-primary">Buscar</button>
				                <button id="index-reset" type="reset" class="btn btn-default pull-right">Limpar</button>
							</div>
						</div>

				    </div>
				</div>
	    	</form>
	    </div>
	</div>
	@foreach($appointments as $a)
		<div class="row">
	        <div class="col-lg-12">

	        	<div class="col-lg-3 col-sm-6">
	        		@if($a->agendable_type == 'Clinic\Appointment')
		            	<div class="box box-{{$a->status == 'em atendimento' ? 'atendimento' : $a->status}}">
		            @else
	            		<div>
		            @endif
			            	@if($a->agendable_type == 'Clinic\Appointment')
				                <div class="box-body">
				                	<ul class="list-unstyled">
				                		<li class="pull-right"><a target="_blank" href="/agenda/{{$a->id}}">Visualizar #{{$a->id}}</a></li>
					                	<li>Profissional: {{$a->doctor->firstName}} {{$a->doctor->lastName}}</li>
					                	<li>Especialidade: {{$a->doctor->roles->first()->jobTitle}}</li>
					                	<li>Data: {{$a->getFormatedDate()}} às {{$a->getFormatedHour()}}</li>
				                		@if($a->agendable_id != null && $a->agendable->requests != null)
				                			<li><a target="_blank" href='/appointments/{{$a->agendable_id}}'>Exames Solicitados</a></li>
				                		@endif
					                	<li>Status: <span class="label {{$a->status}}">{{$a->status}}</span></li>
				                	</ul>
				                </div>
			               	@endif
		            	</div>
		        </div>

		        <div class="col-lg-3 col-sm-6">
		            @if($a->agendable_type == 'Clinic\Exam')
		            	<div class="box box-{{$a->status == 'em atendimento' ? 'atendimento' : $a->status}}">
		            @else
		            	<div>
		            @endif
		               	@if($a->agendable_type == 'Clinic\Exam')
			                

			                <div class="box-body">
			                	<ul class="list-unstyled">
			                		<li class="pull-right"><a target="_blank" href="/agenda/{{$a->id}}">Visualizar #{{$a->id}}</a></li>
			                		@if($a->agendable == null)
				                		<li>Exame: {{$type->where('id',$a->exam)->first()->type}}</li>
				                	@else
				                		<li>Exame: {{$a->agendable->type->type}}</li>
				                	@endif
				                	<li>Profissional: {{$a->doctor->firstName}} {{$a->doctor->lastName}}</li>
				                	<li>Especialidade: {{$a->doctor->roles->first()->jobTitle}}</li>
				                	<li>Data: {{$a->getFormatedDate()}} às {{$a->getFormatedHour()}}</li>
				                	@if( ($a->agendable_id && $a->agendable->appointment_id) != null)
			                			<li>Visualizar: <a href='/appointments/{{$a->agendable->appointment_id}}'>Consulta</a></li>
			                		@endif
				                	<li>Status: <span class="label {{$a->status}}">{{$a->status}}</span></li>
				                </ul>
			                </div>
		               	@endif
		            </div>
		        </div>
	        </div>
    	</div>
    @endforeach
@endsection