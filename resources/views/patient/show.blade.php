@extends('layouts.master')

@section('content')
    
    @component('layouts.box.show')
        
        @slot('title')
            {{$person->firstName}} {{$person->lastName}}
            @if($person->status == 0)
                <span class="label label-danger">desativado</span>
            @endif
        @endslot

        @slot('url')
            /patients/{{$person->id}}/edit
        @endslot
        
        @slot('boxBody')

            <ul class="list-unstyled">
                <li>Idade: {{$person->birthday}}</li>
                <li>Sexo: {{$person->gender == 'male' ? 'Masculino' : 'Feminino'}}</li>
                <li>CPF: {{$person->cpf}}</li>
                <li>RG: {{$person->rg}}</li>
                <li>Endereço: {{$person->address}}</li>
                <li>Complemento: {{$person->secondaryAddress}}</li>   
                <li>E-mail: {{$person->contact->email}}</li>
                <li>Celular: {{$person->contact->phone}}</li>
                <li>Telefone: {{$person->contact->landline}}</li>
                @if( \Auth::user()->person->roles->first()->id == 2 )
                    <li><a href="/patient/appointments/{{$person->id}}">Histórico de Agendamentos</a></li>
                @endif
                @if( \Auth::user()->person->roles->first()->category_id == 3 && $person->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty())
                    <li><a target="_blank" href="/history/{{$person->patient_schedule->where('doctor_id',\Auth::user()->person->id)->first()['id']}}">Histórico Clínico</a></li>
                @endif
                @if( \Auth::user()->person->roles->first()->category_id == 5 && $person->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty())
                    <li><a href="/exam/{{$person->id}}">Histórico de exames</a></li>
                @endif
            </ul>
            <hr>
            <div id="confirm-remove">
                @if($person->status == 1)
                    <button id="confirm-remove-btn" class="btn btn-danger">Desativar paciente</button>
                @else
                    <button id="confirm-remove-btn" class="btn btn-success">Ativar paciente</button>
                @endif
            </div>

            <div id="form-remove" hidden>
                <form action="/patients/{{$person->id}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}

                    @if($person->status == 1)
                        <h4 id='confirm-question' class='text-danger'><strong>Se continuar, o paciente será desativado. Continuar?</strong></strong></h4>
                    @else
                        <h4 id='confirm-question' class='text-danger'><strong>Se continuar, o paciente será ativado. Continuar?</strong></strong></h4>
                    @endif
                    
                    <button class="btn btn-success">Sim</button>
                    <button id="confirm-remove-btn-no" type="button" class="btn btn-danger">Não</button>
                </form>
            </div>
        @endslot

    @endcomponent

@endsection