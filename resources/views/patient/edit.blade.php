@extends('layouts.master')

@section('content')

	@component('layouts.box.edit')

		@slot('skin')
            warning
        @endslot

        @slot('url')
        	/patients/{{$person->id}}
        @endslot

        @slot('firstName')
        	{{$person->firstName}}
        @endslot

		@slot('lastName')
			{{$person->lastName}}
		@endslot

		@slot('birthday')
			{{$person->birthday}}
		@endslot

		@slot('cpf')
			{{$person->cpf}}
		@endslot

		@slot('rg')
			{{$person->rg}}
		@endslot

		@slot('gender')
			{{$person->gender}}
		@endslot

		@slot('postcode')
			{{$person->postcode}}
		@endslot

		@slot('streetName')
			{{$person->streetName}}
		@endslot

		@slot('number')
			{{$person->number}}
		@endslot

		@slot('address')
			{{$person->address}}
		@endslot

		@slot('secondaryAddress')
			{{$person->secondaryAddress}}
		@endslot

		@slot('email')
			{{$person->contact->email}}
		@endslot

		@slot('phone')
			{{$person->contact->phone}}
		@endslot

		@slot('landline')
			{{$person->contact->landline}}
		@endslot

	@endcomponent

@endsection