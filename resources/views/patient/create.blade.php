@extends('layouts.master')

@section('content')

	@component('layouts.box.create')

		@slot('skin')
            primary
        @endslot

		@slot('url')
			/patients
		@endslot

	@endcomponent

@endsection