@extends('layouts.master')

@section('content')

    @component('layouts.box.index')

        @slot('title')
            Pacientes
        @endslot

        @slot('url')
            /patients/create
        @endslot

        @slot('searchUrl')
            /search/patient
        @endslot

        @slot('tbody')

            @if(request()->path() == 'patients')
                {{ $patients->links() }}
            @endif

            @foreach($patients as $p)
                <tr>
                    <td>
                        <button id="datatable-btn-close" class='datatable-btn glyphicon glyphicon-plus'></button>
                        {{$p->firstName}} {{$p->lastName}}
                        @if($p->status == 0)
                            <span class="label label-danger">desativado</span>
                        @endif
                    </td>
                    <td>
                        <div style="float:right;">
                            <a href="/patients/{{$p->id}}"><span class="fa fa-search"> Visualizar</span></a>
                        </div>
                    </td>                                                     
                </tr>
                <tr hidden>
                    <td colspan="3">
                        <ul class="list-unstyled">
                            <li>Idade: {{$p->birthday}}</li>
                            <li>Sexo: {{$p->gender == 'male' ? 'Masculino' : 'Feminino'}}</li>
                            <li>Celular: {{$p->contact->phone}}</li>
                            <li>Telefone: {{$p->contact->landline}}</li>
                            @if( \Auth::user()->person->roles->first()->category_id == 2 )
                                <li><a href="/patient/appointments/{{$p->id}}">Histórico de Agendamentos</a></li>
                            @endif
                            @if( \Auth::user()->person->roles->first()->category_id == 3 && $p->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty())
                                <li><a target="_blank" href="/history/{{$p->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->first()['id']}}">Histórico Clínico</a></li>
                            @endif
                            @if( \Auth::user()->person->roles->first()->category_id == 5 && $p->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty())
                                <li><a href="/exam/{{$p->id}}">Histórico de exames</a></li>
                            @endif
                        </ul>
                    </td>
                </tr>
            @endforeach

        @endslot

    @endcomponent

    @include('layouts.box.back')
    
@endsection                                    