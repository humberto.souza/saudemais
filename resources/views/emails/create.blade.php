<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
Prezad{{$agenda->patient->gender == 'male' ? 'o' : 'a'}} {{$agenda->patient->firstName}},
<p>@if($agenda->agendable_type == 'Clinic\Appointment')
	Este e-mail é para confirmar o agendamento da sua próxima consulta.

	<p><strong>Profissional:</strong> {{$agenda->doctor->firstName}} {{$agenda->doctor->lastName}}</p>

	<p><strong>Especialidade:</strong> {{$agenda->doctor->roles->first()->jobTitle}}</p>

@else
	Este e-mail é para confirmar o agendamento do seu próximo exame.

	<p><strong>Profissional:</strong>  {{$agenda->doctor->firstName}} {{$agenda->doctor->lastName}}</p>

	<p><strong>Exame:</strong> {{$type->where('id',$agenda->exam)->first()->type}}</p>

@endif</p>


<p><strong>Data:</strong> {{$agenda->getFormatedDate()}}</p>

<p><strong>Hora:</strong> {{$agenda->start}}</p>

<p>Atenciosamente,</p>

<p>Saúde Mais</p>


</body>
</html>