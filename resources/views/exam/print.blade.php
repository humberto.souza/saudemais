@extends('layouts.master')

@section('content')

    <div class="row printable">
        <div class="col-lg-8">

            <div id="print-box" class="box box-solid box-primary">
                
                <div id="print-header" class="box-header with-border">
                    <h4 class="box-title">Saúde Mais <span class="fa fa-stethoscope"></span></h4>
                    <div id="btn-print" class="pull-right">
                        <span class="fa fa-print"> Imprimir</span>
                    </div>
                </div>

                <h4>Paciente - {{ $exam->agenda->first()->patient->firstName ." ". $exam->agenda->first()->patient->lastName }}</h4>
                <p>Responsável:</p>
                <p><span class="fa fa-user-md"></span> {{$exam->agenda->first()->doctor->firstName}} {{$exam->agenda->first()->doctor->lastName}} | {{$exam->agenda->first()->doctor->roles->first()->jobTitle}}</p>

                    <div class="box-body">
                        <div class="form-group">
                            <h3>Resultado do Exame - {{$exam->type->type}}</h3>
                            <br/>
                            <div class="print-text">
	                            <fieldset>
                                 <legend>Observações</legend>   
	                               <p>{{$exam->observation}}</p>
                                </fieldset>
                                <fieldset>
                                    <legend>Resultado</legend>
	                                <p>{{$exam->result}}</p>
                                </fieldset>
                            </div>
                        </div>
                    </div>

                <hr id="signature">
                <p>{{\Carbon\Carbon::today()->format('d/m/Y')}}</p>
                <p><span class="fa fa-user-md"></span> Dr. {{ \Auth::user()->person->firstName ." ". \Auth::user()->person->lastName }} | {{\Auth::user()->person->roles->first()->jobTitle}}</p>

                <ul class="list-unstyled">
                    <li><span class="fa fa-envelope-o"></span> {{\Auth::user()->person->contact->email}}</li>
                    <li><span class="fa fa-whatsapp"></span> {{\Auth::user()->person->contact->phone}}</li>
                </ul>

            </div>

        </div>
    </div>

@endsection