@extends('layouts.master')

@section('content')
    
    @component('layouts.box.show')
        
        @slot('title')
            {{$exam->agenda->first()->patient->firstName}} {{$exam->agenda->first()->patient->lastName}}
        @endslot

        @slot('url')
           /exams/{{$exam->id}}/edit
        @endslot
        
        @slot('boxBody')
            <div class="table-responsive">
                <table class="table table-hover">
                    <tr>
                        <th>ID</th>
                        <th>Exame</th>
                        <th>Observações</th>
                        <th>Resultado</th>
                    </tr>
                    <tr>
                        <td>{{$exam->id}}.</td>
                        <td>{{$exam->type->type}}</td>
                        <td>{{$exam->observation}}</td>
                        <td>{{$exam->result}}</td>
                    </tr>
                </table>
            </div>
        @endslot

    @endcomponent

@endsection