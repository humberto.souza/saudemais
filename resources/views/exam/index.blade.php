@extends('layouts.master')

@section('content')

<div class="row">
    <div class="col-lg-8">
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                <div class="info-box">
                    <!-- Apply any bg-* class to to the icon to color it -->
                    <span class="info-box-icon bg-blue"><i class="fa fa-stethoscope"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Histórico de Exames</span>
                        <span class="info-box-number">{{$exams->first()->patient->firstName}} {{$exams->first()->patient->lastName}}</span>
                    </div>
                </div>
            </div>
        </div>
            @foreach($exams as $e)
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>Resultado de Exames</strong></h3>
                        <a href="/print/exam/{{$e->agendable->id}}" target="_blank"><span class="fa fa-print"> imprimir</span></a>
                        
                        <div class="box-tools pull-right">
                            <a target="_blank" href="/exams/{{$e->agendable->id}}/edit"><span class="fa fa-pencil"> Editar</span></a>
                        </div>
                         

                    </div>
                    <div class="box-body">
                       
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <tr>
                                    <th>ID</th>
                                    <th>Exame</th>
                                    <th>Observações</th>
                                    <th>Resultado</th>
                                </tr>
                                <tr>
                                    <td>{{$e->agendable->id}}.</td>
                                    <td>{{$e->agendable->type->type}}</td>
                                    <td>{{$e->agendable->observation}}</td>
                                    <td>{{$e->agendable->result}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="box-footer">
                        <div class="pull-right">
                            Atendido em {{$e->getFormatedDate()}} 
                        </div>    
                    </div>
                    

                </div>   
            @endforeach
    </div>
</div>

@endsection