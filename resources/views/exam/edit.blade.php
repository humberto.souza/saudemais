@extends('layouts.master')

@section('content')

	@if($flash = session('message'))
    	<div class="alert alert-info alert-dismissible">
	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	            <span aria-hidden="true">&times;</span>
	        </button>
	        {{$flash}}   
	    </div>
	@endif
    <div class="row">
        <div class="col-lg-8">
            <div id="exam-create-box" class="box box-primary">
               
                <div class="box-header with-border">
                    <h4 class="box-title">{{ $exam->agenda->first()->patient->firstName ." ". $exam->agenda->first()->patient->lastName }}</h4>
                    <p>{{$exam->agenda->first()->patient->birthday}}</p>
                    <ul class="list-unstyled">
                        <li>Dt nascimento: {{$exam->agenda->first()->patient->date}}</li>
                        <li>Telefone: {{$exam->agenda->first()->patient->contact->landline}}</li>
                        <li>Celular: {{$exam->agenda->first()->patient->contact->phone}}</li>
                        <li>E-mail: {{$exam->agenda->first()->patient->contact->email}}</li>
                    </ul>
                </div>

                <div class="box-body">
                    <form action="/exams/{{$exam->id}}" method="POST">

                        {{ csrf_field() }}

                        {{ method_field('PATCH') }}

                        <div class="form-group">
                            <label for="observation">Observação</label>
                            <textarea rows="4" id="observation" name="observation" class="form-control">{{$exam->observation}}</textarea>
                        </div>
                        
                        <div id="printable" class="form-group">
                            <label for="result">Resultado</label>
                            <textarea rows="10" id="result" name="result" type="textarea" class="form-control" required>{{$exam->result}}</textarea>

                        </div>

                        <button type="submit" class="btn btn-warning">Alterar</button>

                    </form>
                </div>
            </div>

            @include('layouts.box.back')

        </div>

    </div>

@endsection