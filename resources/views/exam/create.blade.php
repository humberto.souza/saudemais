@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-8">
            <div id="appointment-create-box" class="box box-primary">
               
                <div class="printable box-header with-border">
                    <h4 class="box-title">{{ $patient->firstName ." ". $patient->lastName }}</h4>
                    <p>{{$patient->birthday}}</p>
                    <ul class="list-unstyled">
                        <li>Dt nascimento: {{$patient->date}}</li>
                        <li>Telefone: {{$patient->contact->landline}}</li>
                        <li>Celular: {{$patient->contact->phone}}</li>
                        <li>E-mail: {{$patient->contact->email}}</li>
                    </ul>
                </div>

                <div class="box-body">
                    <form action="/exams" method="POST">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <h4>{{$type->type}}</h4>
                            <input type="hidden" value="{{$agenda->id}}" name="agenda_id">
                        </div>  

                        <div class="form-group">
                            <label for="observation">Observação</label>
                            <textarea rows="4" id="observation" name="observation" class="form-control"></textarea>
                        </div>
                        
                        <div class="form-group">
                            <label for="result">Resultado</label>
                            <textarea rows="8" id="result" name="result" type="textarea" class="form-control" required></textarea>
                        </div>

                        <button type="submit" class="btn btn-default">Finalizar</button>
                    
                    </form>
                </div>

            </div>
        </div>
    </div>

        

@endsection