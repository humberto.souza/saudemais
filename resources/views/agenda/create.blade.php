@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-4">
            <div id="agenda-create-box" class="box box-primary">
                <div class="box-body">
                   
                    <form class="schedule" action="/agenda" method="POST">

                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-8 col-sm-8">
                                <div class="form-group">
                                    <input id="created_by" type="hidden" name="created_by" value="{{ Auth::user()->person->id}}">
                                    @isset($appointment)
                                        <input id="appointment_id" type="hidden" name="appointment_id" value="{{$appointment->id}}">
                                    @endisset
                                </div>
                            </div>
                        </div>

                        @if(isset($appointment))
                            <div class="row">

                                <div class="col-md-8 col-sm-8">
                                    <div class="form-group">
                                        <label for="type">Tipo de Agendamento</label>
                                        <select id="type" name="agendable_type" class="form-control" required>
                                            <option value="Clinic\Exam">Exame</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                            @if( !str_contains(request()->path(), 'agenda/request') )
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-question" class="form-group">
                                        <div>
                                            <input id="question" type="checkbox" name="question">
                                            <label for="question">Agendar para médico?</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-patient" class="form-group">
                                        <label for="patient">Paciente</label>
                                            <select id="patient" name="patient_id" class="form-control" required>
                                                <option value="{{$appointment->agenda->first()->patient->id}}" selected>{{$appointment->agenda->first()->patient->firstName}} {{$appointment->agenda->first()->patient->lastName}}</option>
                                            </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-exam" class="form-group">
                                        <label for="exam">Tipos de Exame</label>
                                        <select id="exam" name="exam" class="form-control" required>
                                            <option disabled selected>Selecione</option>
                                            <option value="{{$exam->id}}">{{$exam->type}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-doctor" class="form-group" required hidden>
                                        <label for="doctor">Profissional</label>
                                        <select id="doctor" name="doctor_id" class="form-control" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @else
                        <!-- if Appoitment is not set -->
                            <div class="row">
                                
                                <div class="col-md-8 col-sm-8">
                                    <div class="form-group">
                                        <label for="type">Tipo de Agendamento</label>
                                        <select id="type" name="agendable_type" class="form-control" required>
                                            <option disabled selected>Selecione</option>
                                            <option value="Clinic\Appointment">Consulta</option>
                                            <option value="Clinic\Exam">Exame</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <button id="reset" type="reset" class="btn btn-default pull-right">Limpar</button>
                                    </div>
                                </div>

                            </div>
                            @if( !str_contains(request()->path(), 'agenda/request') )
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-question" class="form-group">
                                        <div>
                                            <input id="question" type="checkbox" name="question">
                                            <label for="question">Agendar para médico?</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-patient" class="form-group" hidden>
                                        <label for="patient">Paciente</label>
                                        <select id="patient" name="patient_id" class="form-control" required>
                                            <option disabled selected>Selecione</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-jobTitle" class="form-group" hidden>
                                        <label for="jobTitle">Especialidade</label>
                                        <select id="jobTitle" class="form-control" required>
                                            <option disabled selected>Selecione</option>
                                            @foreach($roles as $r)
                                                <option value="{{$r->id}}">{{$r->jobTitle}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-exam" class="form-group" hidden>
                                        <label for="exam">Tipos de Exame</label>
                                        <select id="exam" name="exam" class="form-control" required>
                                            <option disabled selected>Selecione</option>
                                            @foreach($exams as $e)
                                                <option value="{{$e->id}}">{{$e->type}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="field-doctor" class="form-group" required hidden>
                                        <label for="doctor">Profissional</label>
                                        <select id="doctor" name="doctor_id" class="form-control" required>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <!-- end if Appoitment is not set -->
                        <div class="row">
                            <div class="col-sm-3 col-md-6">
                                <div id="field-date" class="form-group" hidden>
                                    <label for="date">Data</label>
                                    <input id="date" type="hidden" name="date">
                                    <p id="p-date" class="form-control"></p>
                                </div>
                            </div>
                        
                            <div class="col-sm-3 col-md-6">
                                <div id="field-time" class="form-group" hidden>
                                    <label for="time">Hora</label>
                                    <input id="time" name="start" type="time" class="form-control">
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            
            @include('layouts.box.schedules')

        </div>

        <div class="col-lg-8">
            <div id="box-calendar" class="box box-primary" hidden>
                <div id='calendar'></div>
            </div>
        </div>

    </div>

@endsection