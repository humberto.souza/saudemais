@extends('layouts.master')

@section('content')

	@component('layouts.box.show')

		@slot('title')
			{{ $agenda->patient->firstName }} {{ $agenda->patient->lastName }} <span class="label {{ $agenda->status }}">{{ $agenda->status }}</span>
		@endslot

		@slot('url')
			/agenda/{{$agenda->id}}/edit
		@endslot

		@slot('boxBody')
			<ul class="list-unstyled">
				<li>Data: {{ $agenda->getFormatedDate() }} às {{ $agenda->getFormatedHour() }} </li>
                <li>Profissional: {{ $agenda->doctor->firstName }} {{ $agenda->doctor->lastName }} </li>
                <li>Especialidade: {{ $agenda->doctor->roles->first()->jobTitle }}</li>
                @if($type != null)
                	<li>Tipo de Exame: {{$type->type}}</li>
                	@if($agenda->status == 'finalizado' && \Auth::user()->person->roles->first()->category_id == 5)
                		<li>Visualizar: <a href="/exams/{{$agenda->agendable->id}}">Resultado</a></li>
                        <li><a href="/exam/{{$agenda->patient->id}}">Histórico de Exames</a></li>
                	@endif
                @else

               		@if($agenda->count > 0)
               			@if( Auth::user()->person->id == $agenda->doctor_id )
	                		<li><a target="_blank" href="/history/{{$agenda->id}}">Histórico Clínico</a></li>
	                	@elseif( \Auth::user()->person->roles->first()->id == 2)
                                <li><a href="/patient/appointments/{{$agenda->patient->id}}">Histórico de Agendamentos</a></li>
                        @endif
	                @endif

	                @if($agenda->status == 'finalizado')
	                	
	                	@if(Auth::user()->person->id == $agenda->doctor_id)
		                	<li>Visualizar: <a href="/appointments/{{$agenda->agendable->id}}">Prescrição</a></li>

			                <li>Emitir: <a target="_blank" href="/print/appointment/prescription">Receita</a>
			                		@if($agenda->agendable->requests != null)
		                				| <a target="_blank" href="/print/appointment/exams">Exames Solicitados</a>
                					@endif
			                </li>
			            @else

		                	@if($agenda->agendable_type == 'Clinic\Appointment' && $agenda->agendable->requests != null)
	                			<li><a href="/appointments/{{$agenda->agendable->id}}">Agendar exames</a></li>
	                		@endif

	                	@endif

	                @endif

		        @endif
            </ul>
            <tfoot>
            	<hr>
            	<p>Agendado dia {{ $agenda->created_date }} às {{ $agenda->created_time }} por {{ $agenda->scheduled->firstName }} {{ $agenda->scheduled->lastName }}</p>
            </tfoot>
        @endslot
    
	    @slot('changeStatus')

	    	<!-- Must be a recepcionist, the status needs to be "marcado" and needs to be the current day-->
	    	@if( Auth::user()->person->roles->first()->category_id == 2 && $agenda->status == 'marcado' && date('Y-m-d') == $agenda->date)
				<form action="/changeAgendaStatus/{{$agenda->id}}" method="POST">
					
					{{ csrf_field() }}
		            {{ method_field('PATCH') }}

		            <input value="aguardando" type="hidden" name="status">
		    		<button class="btn btn-warning">Aguardar</button>
		    	</form>
			@endif

	    	<!-- Must be the same doctor/date scheduled and the status needs to be, "wating"-->
	    	@if( Auth::user()->person->id == $agenda->doctor_id && $agenda->status == 'aguardando' && date('Y-m-d') == $agenda->date)
				<form action="/changeAgendaStatus/{{$agenda->id}}" method="POST">
		    		
		    		{{ csrf_field() }}
		            {{ method_field('PATCH') }}

		            <input value="em atendimento" type="hidden" name="status">
		    		<button class="btn btn-primary">Atender</button>
		    	</form>
			@endif

			@if( Auth::user()->person->id == $agenda->doctor_id && $agenda->status == 'em atendimento' && date('Y-m-d') == $agenda->date)
				<form action="/changeAgendaStatus/{{$agenda->id}}" method="POST">
					{{ csrf_field() }}
		            {{ method_field('PATCH') }}
					
		            <input value="continuar atendimento" type="hidden" name="status">
		    		<button class="btn btn-primary">Continuar atendimento</button>
		    	</form>
			@endif

			@if( ( Auth::user()->person->id == $agenda->doctor_id || Auth::user()->person->roles->first()->category_id == 2 ) && $agenda->status == 'marcado' && date('Y-m-d') > $agenda->date)
				<form action="/changeAgendaStatus/{{$agenda->id}}" method="POST">

					{{ csrf_field() }}
		            {{ method_field('PATCH') }}

					<input value="faltou" type="hidden" name="status">
		    		<button class="btn faltou">Não compareceu</button>
		    	</form>
			@endif
			
	    @endslot
		
	@endcomponent

@endsection