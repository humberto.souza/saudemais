@extends('layouts.master')

@section('content')
  
    <div class="row">
        <div class="col-lg-4">
            <div class="box box-primary">
                <div class="box-body">
                                        
                    <form class="schedule" action="/agenda/{{$agenda->id}}" method="POST">

                        {{ csrf_field() }}

                        {{ method_field('PATCH') }}

                        <div class="row">
                            <div class="col-md-8 col-sm-8">
                                <div class="form-group">
                                    <input id="agenda_id" type="hidden" name="agenda_id" value="{{ $agenda->id}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-8 col-sm-8">
                                <div class="form-group">
                                    <label for="type">Tipo de Agendamento</label>
                                    <select id="type" name="agendable_type" class="form-control" required>
                                        <option disabled>Selecione</option>
                                        <option value="{{$agenda->agendable_type}}" selected>{{$agenda->type}}</option>
                                        @if($agenda->agendable_type == 'Clinic\Exam')
                                            <option value="Clinic\Appointment">Consulta</option>
                                        @else
                                            <option value="Clinic\Exam">Exame</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div id="field-question" class="form-group">
                                        <div>
                                            <input id="question" type="checkbox" name="question" {{$isDoctor >= 1 ? 'checked' : ''}}>
                                            <label for="question">Agendar para médico?</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="field-patient" class="form-group">
                                    <label for="patient">Paciente</label>
                                    <select id="patient" name="patient_id" class="form-control" required>
                                        <option disabled>Selecione</option>
                                        @foreach($patients as $p)
                                            @if($p->id == $agenda->patient->id)
                                                <option value="{{$agenda->patient->id}}" selected>{{$agenda->patient->firstName}} {{$agenda->patient->lastName}}</option>
                                                @continue;
                                            @endif
                                            <option value="{{$p->id}}">{{$p->firstName}} {{$p->lastName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="field-jobTitle" class="form-group" {{$agenda->agendable_type == 'Clinic\Exam' ? 'hidden' : ''}}>
                                    <label for="jobTitle">Especialidade</label>
                                    <select id="jobTitle" class="form-control" required>
                                        <option disabled>Selecione</option>
                                        @foreach($roles as $r)
                                            @if($r->id == $agenda->doctor->roles->first()->id)
                                                <option value="{{$agenda->doctor->roles->first()->id}}" selected>{{$agenda->doctor->roles->first()->jobTitle}}</option>
                                                @continue;
                                            @endif
                                            <option value="{{$r->id}}">{{$r->jobTitle}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div id="field-exam" class="form-group" {{$agenda->agendable_type == 'Clinic\Exam' ? '' : 'hidden'}}>
                                        <label for="exam">Tipos de Exame</label>
                                        <select id="exam" name="exam" class="form-control" required>
                                            <option disabled selected>Selecione</option>
                                            @if(isset($exam_type))
                                                @foreach($exams as $e)
                                                    @if($exam_type->type == $e->type)
                                                    <option value="{{$exam_type->id}}" selected>{{$exam_type->type}}</option>
                                                        @continue;
                                                    @endif
                                                    <option value="{{$e->id}}">{{$e->type}}</option>
                                                @endforeach
                                            @else
                                                @foreach($exams as $e)
                                                    <option value="{{$e->id}}">{{$e->type}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="field-doctor" class="form-group" required>
                                    <label for="doctor">Profissional</label>
                                    <select id="doctor" name="doctor_id" class="form-control" required>
                                        <option disabled>Selecione</option>
                                        @foreach($doctors as $d)
                                            @if($d->id == $agenda->doctor->id)
                                                <option value="{{$agenda->doctor_id}}" selected>{{$agenda->doctor->firstName}} {{$agenda->doctor->lastName}}</option>
                                                @continue;
                                            @endif
                                            <option value="{{$d->id}}">{{$d->firstName}} {{$d->lastName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 col-md-6">
                                <div id="field-date" class="form-group">
                                    <label for="date">Data</label>
                                    <input id="date" value="{{$agenda->date}}" type="hidden" name="date">
                                    <p id="p-date" class="form-control">{{$agenda->pdate}}</p>
                                </div>
                            </div>
                        
                            <div class="col-sm-3 col-md-6">
                                <div id="field-time" class="form-group">
                                    <label for="time">Hora</label>
                                    <input id="time" name="start" value="{{$agenda->start}}" type="time" class="form-control">
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            @include('layouts.box.schedules')

            @if( ( Auth::user()->person->roles->first()->category_id == 2 || Auth::user()->person->roles->first()->category_id == 3 ) && $agenda->status == 'marcado')
            <form id="btn-cancel-agenda" action="/agenda/{{$agenda->id}}" method="POST">

                {{ csrf_field() }}

                {{ method_field('DELETE') }}

                <button class="btn btn-danger">Cancelar agendamento</button>
            </form>
            @endif

            @include('layouts.box.back')
            
        </div>



        <div class="col-lg-8">
            <div id="box-calendar" class="box box-primary">
                <div id='calendar'></div>
            </div>
        </div>

    </div>



@endsection