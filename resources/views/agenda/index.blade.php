@extends('layouts.master')

@section('content')

    <div class="row">
        <div class="col-lg-8">
            <div class="box box-primary">
            	<div id="category_id" hidden>{{$category}}</div>
            	<div id="employee_id" hidden>{{$employee}}</div>
                <div id='index-calendar'></div>
            </div>
        </div>

        <form>
	        <div class="col-lg-4">
		        <div id="box-filter" class="box box-primary">
				    <div class="box-body">
				        <label for="column">Filtrar por</label>
				        @if( Auth::user()->person->roles->first()->category_id == 2)
					        <div class="row">
						        <div class="form-group col-sm-6 col-lg-10 ">
						        	<label for="agendable-filter">Tipo de Agendamento</label>
					                <select id="agendable-filter" class="form-control" name="agendable-filter">
					                    <option value="" disabled selected>Selecione</option>
					                    <option value="Clinic\Appointment">Consulta</option>
					                    <option value="Clinic\Exam">Exame</option>
					                </select>
					            </div>
					        </div>
				        @endif
			            <div class="row">
					        <div class="form-group col-sm-6 col-lg-10 ">
					        	<label for="status-filter">Status</label>
				                <select id="status-filter" class="form-control" name="status-filter">
				                    <option value="" disabled selected>Selecione</option>
				                    <option value="marcado">Marcado</option>
				                    <option value="aguardando">Aguardando</option>
				                    <option value="em atendimento">Em atendimento</option>
				                    <option value="finalizado">Finalizado</option>
				                    <option value="faltou">Não compareceu</option>
				                    <option value="cancelado">Cancelado</option>
				                </select>
				            </div>
				        </div>

			            <div class="row">
				            <div class="form-group col-sm-6 col-lg-10 ">
					        	<label for="patient-filter">Paciente</label>
					        	<span class='pull-right'>
				               		<input id="question-filter" type="checkbox"> É um médico?
				               	</span>
				                <select id="patient-filter" class="form-control" name="patient_id">
				                    <option value="" disabled selected>Selecione</option>
				                	@foreach($patients as $p)
				                    	<option value="{{$p->id}}">{{$p->firstName}} {{$p->lastName}}</option>
				                    @endforeach
				                </select>
				            </div>
				        </div>
				        @if( Auth::user()->person->roles->first()->category_id == 2)
				            <div class="row">
					            <div class="form-group col-sm-6 col-lg-10 ">
						        	<label for="doctor-filter">Profissional</label>
					                <select id="doctor-filter" class="form-control" name="doctor_id">
					                    <option value="" disabled selected>Selecione</option>
					                    @foreach($doctors as $d)
					                    	<option value="{{$d->id}}">{{$d->firstName}} {{$d->lastName}} </option>
					                    @endforeach
					                </select>
					            </div>
				            </div>
			            @endif						
						<div class="row">
				            <div class="form-group col-sm-6 col-lg-10 ">
								<button type="button" id="btn-filter" class="btn btn-primary">Filtrar</button>
				                <button id="index-reset" type="reset" class="btn btn-default pull-right">Limpar</button>
							</div>
						</div>

				    </div>
				</div>
		    </div>
	    </form>
	</div>

@endsection