@extends('layouts.master')

@section('content')
    
    @component('layouts.box.show')
            
        @slot('title')
            {{$person->firstName}} {{$person->lastName}} 
            @if($person->status == 0)
                <span class="label label-danger">desativado</span>
            @endif
        @endslot

        @slot('url')
            /employees/{{$person->id}}/edit
        @endslot
        
        @slot('boxBody')
            <ul class="list-unstyled">
                <li>Especialidade: {{$person->roles->first()->jobTitle}}</li>
                <li>Idade: {{$person->birthday}}</li>
                <li>Sexo: {{$person->gender == 'male' ? 'Masculino' : 'Feminino'}}</li>
                <li>CPF: {{$person->cpf}}</li>
                <li>RG: {{$person->rg}}</li>
                <li>Endereço: {{$person->address}}</li>
                <li>Complemento: {{$person->secondaryAddress}}</li>   
                <li>E-mail: {{$person->contact->email}}</li>
                <li>Celular: {{$person->contact->phone}}</li>
                <li>Telefone: {{$person->contact->landline}}</li>
                @if( \Auth::user()->person->roles->first()->id == 2)
                    <li><a href="/patient/appointments/{{$person->id}}">Histórico de Agendamentos</a></li>
                @endif
                @if( \Auth::user()->person->roles->first()->category_id == 3 && $person->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty())
                    <li><a target="_blank" href="/history/{{$person->patient_schedule->where('doctor_id',\Auth::user()->person->id)->first()['id']}}">Histórico Clínico</a></li>
                @endif
                @if( \Auth::user()->person->roles->first()->category_id == 5 && $person->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty())
                    <li><a href="/exam/{{$person->id}}">Histórico de exames</a></li>
                @endif

            </ul>
            @if($person->expedients->isNotEmpty())
                <h4>Horário de Expediente</h4>
                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-lg-4">
                        <table class="table table-hover table-striped table-condensed">
                            <tr>
                                <th>#</th>
                                <th>das</th>
                                <th>às</th>
                            </tr>
                            @foreach($person->expedients->sortBy('weekday') as $e)
                                <tr>
                                    <td>{{$weekday[$e->weekday]}}</td>
                                    <td>{{$e->start}}</td>
                                    <td>{{$e->end}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            @endif

            @if(\Auth::user()->person->roles->first()->category_id == 1)
                <hr>
                <div id="confirm-remove">
                    @if($person->status == 1)
                        <button id="confirm-remove-btn" class="btn btn-danger">Desativar usuário</button>
                    @else
                        <button id="confirm-remove-btn" class="btn btn-success">Ativar usuário</button>
                    @endif
                </div>
                <div id="form-remove" hidden>
                    <form action="/employees/{{$person->id}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        @if($person->status == 1)
                            <h4 id='confirm-question' class='text-danger'><strong>Se continuar, o usuário será desativado. Continuar?</strong></strong></h4>
                        @else
                            <h4 id='confirm-question' class='text-danger'><strong>Se continuar, o usuário será ativado. Continuar?</strong></strong></h4>
                        @endif

                        <button class="btn btn-success">Sim</button>
                        <button id="confirm-remove-btn-no" type="button" class="btn btn-danger">Não</button>
                    </form>
                </div>
            @endif

        @endslot

    @endcomponent

@endsection