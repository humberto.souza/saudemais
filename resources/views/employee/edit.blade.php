@extends('layouts.master')

@section('content')

	@component('layouts.box.edit')

		@slot('skin')
            warning
        @endslot

		@slot('url')
			/employees/{{$person->id}}
		@endslot

		@slot('firstName')
        	{{$person->firstName}}
        @endslot

		@slot('lastName')
			{{$person->lastName}}
		@endslot

		@slot('birthday')
			{{$person->birthday}}
		@endslot

		@slot('cpf')
			{{$person->cpf}}
		@endslot

		@slot('rg')
			{{$person->rg}}
		@endslot

		@slot('gender')
			{{$person->gender}}
		@endslot

		@slot('postcode')
			{{$person->postcode}}
		@endslot

		@slot('streetName')
			{{$person->streetName}}
		@endslot

		@slot('number')
			{{$person->number}}
		@endslot

		@slot('address')
			{{$person->address}}
		@endslot

		@slot('secondaryAddress')
			{{$person->secondaryAddress}}
		@endslot

		@slot('email')
			{{$person->contact->email}}
		@endslot

		@slot('phone')
			{{$person->contact->phone}}
		@endslot

		@slot('landline')
			{{$person->contact->landline}}
		@endslot

		@slot('foreachRoles')

	    		@foreach($roles as $r)
		    		<option value="{{$r->id}}" {{$person->roles->first()->jobTitle == $r->jobTitle ? 'selected' : ''}}>{{$r->jobTitle}}</option>
		    	@endforeach

		@endslot

		@slot('forWeekdays')

			<fieldset id="officeHours" {{$person->roles->first()->id <= 2 ? 'hidden' : ''}}>
				    	<legend>Horário de Atendimento</legend>
				@foreach($weekday as $i => $weekday)

				    <div class="row">
				        <div class="form-group col-sm-4 col-md-2">
				        	<label>&nbsp;</label>
				     		<div class="input-group">
	      						<div class="input-group-addon">
	               					<input id="{{$weekday}}" class="controlWeekday" type="checkbox" value="{{1+$i}}" name="weekday[]">
	      						</div>
					            <p class="form-control" disabled>{{$weekday}}</p>
				        	</div>
				        </div>

				        <div class="form-group col-sm-4 col-md-2">
				            <label for="{{$weekday}}">De</label>
				            <div class="input-group">
	      						<div class="input-group-addon">
	      							<i class="fa fa-clock-o"></i>
	      						</div>
	      						<input type="time" name="start[]" class="form-control {{$weekday}}" value="{{$e->where('weekday',$i+1)->isNotEmpty() ? $e->where('weekday',$i+1)->first()->start : ''}}" disabled>
	      						
	      					</div>
				        </div>

				        <div class="form-group col-sm-4 col-md-2">
				            <label for="{{$weekday}}">às</label>
				            <div class="input-group">
	      						<div class="input-group-addon">
	      							<i class="fa fa-clock-o"></i>
	      						</div>
				            	<input type="time" name="end[]" class="form-control {{$weekday}}" value="{{$e->where('weekday',$i+1)->isNotEmpty() ? $e->where('weekday',$i+1)->first()->end : ''}}" disabled >
				            </div>
				        </div>

				    </div>

				@endforeach

			</fieldset>

		@endslot

	@endcomponent

@endsection