@extends('layouts.master')

@section('content')

    @component('layouts.box.index')
        
        @slot('title')
            Funcionários
        @endslot

        @slot('url')
            /employees/create
        @endslot

        @slot('searchUrl')
            /search/employee
        @endslot

        @slot('tbody')

            @if(request()->path() == 'employees')
                {{ $employees->links() }}
            @endif

            @foreach($employees as $e)
                <tr>
                    <td>
                        <button id="datatable-btn-close" class='datatable-btn glyphicon glyphicon-plus'></button>
                        {{$e->firstName}} {{$e->lastName}} 
                        @if($e->status == 0)
                            <span class="label label-danger">desativado</span>
                        @endif
                    </td>
                    <td style="border">
                        <div style="float:right;">
                            <a href="/employees/{{$e->id}}" style="float:right"><span class="fa fa-search"> Visualizar</span></a>
                        </div>
                    </td>                              
                </tr>
                <tr hidden>
                    <td colspan="2">
                        <ul class="list-unstyled">
                            <li>Especialidade: {{$e->roles->first()->jobTitle}}</li>
                            <!--<li>Atendimento: </li>-->
                            <li>Endereço: {{$e->address}}</li>
                            <li>Complemento: {{$e->secondaryAddress}}</li>
                            <li>Celular: {{$e->contact->phone}}</li>
                            <li>Telefone: {{$e->contact->landline}}</li>
                            @if( \Auth::user()->person->roles->first()->id == 2 )
                                <li><a href="/patient/appointments/{{$e->id}}">Histórico de Agendamentos</a></li>
                            @endif
                            @if( \Auth::user()->person->roles->first()->category_id == 3 && $e->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty())
                                <li><a target="_blank" href="/history/{{$e->patient_schedule->where('doctor_id',\Auth::user()->person->id)->first()['id']}}">Histórico Clínico</a></li>
                            @endif
                            @if( \Auth::user()->person->roles->first()->category_id == 5 && $e->patient_schedule->where('doctor_id',\Auth::user()->person->id)->where('agendable_id','!=',null)->isNotEmpty() )
                            <li><a href="/exam/{{$e->id}}">Histórico de exames</a></li>
                        @endif
                        </ul>
                    </td>
                </tr>
            @endforeach
            
        @endslot

    @endcomponent

    @include('layouts.box.back')

@endsection