@extends('layouts.master')

@section('content')

	@component('layouts.box.create')

		@slot('skin')
            primary
        @endslot

		@slot('url')
			/employees
		@endslot

		@slot('foreachRoles')

			@foreach($roles as $r)
	    		<option value="{{$r->id}}">{{$r->jobTitle}}</option>
	    	@endforeach

		@endslot

		@slot('forWeekdays')

			@for ($i = 0; $i < 6; $i++)
	            
			    <div class="row">
			        <div class="form-group col-sm-4 col-md-2">
			        	<label>&nbsp;</label>
			     		<div class="input-group">
      						<div class="input-group-addon">
               					<input id="{{$weekday[$i]}}" class="controlWeekday" type="checkbox" value="{{1+$i}}" name="weekday[]">
      						</div>
				            <p class="form-control" disabled>{{$weekday[$i]}}</p>
			        	</div>
			        </div>

			        <div class="form-group col-sm-4 col-md-2">
			            <label for="{{$weekday[$i]}}">De</label>
			            <div class="input-group">
      						<div class="input-group-addon">
      							<i class="fa fa-clock-o"></i>
      						</div>
      						<input type="time" name="start[]" class="form-control {{$weekday[$i]}}" disabled required>
      					</div>
			        </div>

			        <div class="form-group col-sm-4 col-md-2">
			            <label for="{{$weekday[$i]}}">às</label>
			            <div class="input-group">
      						<div class="input-group-addon">
      							<i class="fa fa-clock-o"></i>
      						</div>
			            	<input type="time" name="end[]" class="form-control {{$weekday[$i]}}" disabled required>
			            </div>
			        </div>

		    	</div>

			@endfor

		@endslot

	@endcomponent

@endsection